<?php

/**
 * V3 runner
 * @author xardas
 * @package V3
 * @subpackage Boot
 *
 * Just need to say - I REALLY REALLY LOVE references in PHP5 - keeps track of everything everywhere,
 * never get any object with outdated data - easy to notice with V3_Channel, V3_*User and so on.
 * Really handy, update it in one place (even by fluent interface ->getSth()->getMore()->getFoo()->bar='xxx';
 * and EVERY object in the application knows about the change.
 * Let me say it once more - object references in PHP5 are pure brilliance.
*/

require_once './bootstrap.php';

$objCLI = V3::getModule('CLI');
$blnTerm = $objCore -> daemonCheck( $objCLI );

$blnTerm || $objCore -> executeCommand( $objCLI -> getCommand() );
exit;
<?php
class V3_UpgradeTask extends V3_Task
{
	
	public static $feedUrl = 'http://127.0.0.1/v3_upgrade/feed.php'; // good
	/* _init must return true if everything is fine */
	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '0.5';
		$this -> required_params = array(
		);

		$this -> description = 'Upgrades V3 platform to the latest possible release';
		
		return true;
	}
	
	public function _exec()
	{
		V3::log( 'Checking for updates..');
		
//		$strResult = file_get_contents( self::$feedUrl );
		
        $objCurl = curl_init(); 



        curl_setopt($objCurl, CURLOPT_URL, self::$feedUrl ); 
        curl_setopt($objCurl, CURLOPT_RETURNTRANSFER, 1); 
//		curl_setopt($objCurl, CURLOPT_CONNECTTIMEOUT, 5 );
        $strResult = trim( curl_exec($objCurl) );  
		
        curl_close($objCurl);      
		
		if( empty( $strResult ) ) 
		{
			throw new Exception( 'Connection to V3 upgrade server has failed! Please try again later.' );
		}

		clearstatcache();
		$arrChanged = array();
		foreach( explode( "\n", $strResult ) as $strLine )
		{
			list( $strFile, $strHash, $intMTime ) = explode( ':', $strLine, 3 );

			if( $strHash == '#MSG#')
			{
				if( !empty($arrChanged) )
				{
					V3::log( 'MESSAGE FROM MAINTAINER: ');
					V3::log( $strFile );
					$this -> getModule( 'CLI' ) -> askYesNo( 'Do you want to continue?') or die();
				}
				continue;
			}
			
			if(!file_exists(V3_DIR.'/'.$strFile))
			{
				// totally new file
				$arrChanged[] = array( $strFile, $intMTime, $strHash );
				continue;
			}
			
			$strOldHash = md5_file( V3_DIR.'/'.$strFile);
			$intOldMTime = filemtime(V3_Dir.'/'.$strFile);
			
			if( $strHash != $strOldHash )
			{
				$arrChanged[] = array( $strFile, date('d-m-Y', $intMTime ), $strHash );
			}
		}
		
		$arrHeaders = array(
			'File', 'Release date', 'MD5 signature'
		);
		
		if( empty($arrChanged))
		{
			V3::log( 'Nothing to upgrade, you have the latest V3!');
			return;
		}
		
		
		V3::log( sizeof($arrChanged). ' file(s) to upgrade:' );
		
		echo $this -> getModule( 'CLI' ) -> renderTable( $arrHeaders, $arrChanged );
		
		$this -> getModule( 'CLI' ) -> askYesNo( 'Do you want to commence update?') or die();
		
		foreach( $arrChanged as $arrFile )
		{
			$strFile = $arrFile[0];
			V3::log('Downloading new version of '. $strFile.'...');
			
			$strContent = file_get_contents( self::$feedUrl.'?request='. $strFile );
			
			if( $strContent == 'ERR' )
			{
				throw new Exception('Internal upgrade server error occured, operation aborted');
			}
			$intKey = md5($strContent);
			
			if( $intKey != $arrFile[2] )
			{
				throw new Exception( sprintf( 'MD5 signature for file "%s" (%s) mismatched the expected "%s". Aborting!',
					$strFile, $intKey, $arrFile[2]
				) );
				
				
				return;
			}
			
			if( file_put_contents( $strFile, $strContent ) )
			{
				V3::log( 'Upgraded file: '. $strFile);
			}
			else
			{
				throw new Exception( sprintf( 'Upgrade for file "%s" failed, aborting.', $strFile ) );
			}
		}
		
		V3::log('Upgrade done.');
		
	}
}
?>

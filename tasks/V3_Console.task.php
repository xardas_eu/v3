<?php
class V3_ConsoleTask extends V3_Task
{
	/* _init must return true if everything is fine */
	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '0.9';
		$this -> required_params = array(
			'name' => 'Bot name'
		);

		$this -> description = 'Gives access to V3 internal core console';
		
		return true;
	}
	
	public function _exec()
	{
		stream_set_blocking(STDIN, FALSE);
		
		
		$this -> getCore() -> initialize();
		$strBuf = '';
		echo V3::CRLF.'[>] ';
		while( !V3::getCore() -> isTerminated() )
		{
			V3::castEvent( 'idle' );
			$strRead = fread( STDIN, 1 );
			if( $strRead == chr(10) )
			{
				$strData = trim( $strBuf );
				$strBuf  = '';
			}
			else
			{
				$strBuf .= $strRead;
			}
			
			if( !empty( $strData ) )
			{
				ob_start();
				eval( $strData );
				V3::log( ob_get_clean() );
				echo V3::CRLF. '[>] ';
				$strData = '';
			}
			
			
			usleep( 10000 );
		}
		
		
		
		stream_set_blocking(STDIN, TRUE );
	}
}
?>

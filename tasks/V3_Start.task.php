<?php
class V3_StartTask extends V3_Task
{
	/* _init must return true if everything is fine */
	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '0.0.5';
		$this -> required_params = array(
			'name' => 'Bot name'
		);

		$this -> description = 'Starts the bot';
		
		return true;
	}
	
	public function _exec()
	{
		$this -> getCore() -> initialize();
		while( !V3::getCore() -> isTerminated() )
		{
			V3::castEvent( 'idle' );
			usleep( 30000 );
		}
		
		
		V3::log( 'V3 v.'.V3::BOT_VERSION.' shutting down.' );
	}
}
?>

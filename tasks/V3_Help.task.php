<?php
class V3_HelpTask extends V3_Task
{
    /* _init must return true if everything is fine */

    public function _init()
    {
        $this -> author = 'xardas';
        $this -> version = '0.9';
        $this -> required_params = array(
        );

        $this -> description = 'Displays help screen';

        return true;
    }

    public function _exec()
    {
		$this -> getCore() -> registerModule( 'Debug' );
		V3::log( 'Welcome to the V3 help screen.', V3::INFO );
		V3::log( 'Below is the list of the available commands (tasks):');

		$objCli = $this -> getCore() -> getModule( 'CLI' );

		$arrTableHeaders = array( 'Command', 'Description', 'Parameters' );

		$arrTableData	 = array();


		foreach( glob( V3_DIR.'/tasks/*.task.php' ) as $fileName )
		{
			include_once $fileName;
			$strTaskName = str_replace( array( V3_DIR, '/tasks/', '.task.php' ) ,'', $fileName ). 'Task';
			$objTask = new $strTaskName();
			$objTask -> _init(); /* Sure hope it doesn't screw something! */
			
			$strHumanReadableTaskName /* duh */ = strtolower( str_replace( array( 'V3_', 'Task' ), '', $strTaskName ) );

			$strParams = '';
			foreach( $objTask -> required_params as $strName => $strDescription )
			{
				$strParams .= sprintf( '--%s "%s" ', $strName, $strDescription );
			}

			$arrTableData[] = array( $strHumanReadableTaskName, $objTask -> description, $strParams );
		}


		echo $objCli -> renderTable( $arrTableHeaders, $arrTableData );
		V3::log('Global parameter list (can be used with any command):');

		echo $objCli -> renderTable(
				array( 'Parameter', 'Behaviour', 'Other'),
				array(
					array('--nodebug', 'Don\'t print debug messages'),
					array( '--noverbose', 'Don\'t print verbose messages' ),
					array( '--trace', 'Print call trace for every method (messy!)' ),
					array( '--trace-files', 'Prints file:line pairs after each trace step (even more messy!)' ),
					array( '--quiet (or -q)', 'Be totally quiet, don\'t output anything (almost anything)' ),
					array( '--version (or -v)', 'Prints V3 version and exits', 'Use with no command' ),
					array( '--daemon (or -d)','EXPERIMENTAL: Run in background mode')
					)
				

				);

    }

}
?>

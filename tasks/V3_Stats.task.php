<?php
class V3_StatsTask extends V3_Task
{

	/* _init must return true if everything is fine */
	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '0.0.1';
		$this -> required_params = array();
		$this -> description = 'Provides brief information about PHP and V3';
		return true;
	}

	public function _exec()
	{
		$this -> getCore() -> registerModule( 'Debug' );
		V3::log( 'V3 pre-run statistics' );
		$arrCont = array();
		$arrCont[ 0 ] = array( 'PHP Version', pakeColor::colorize( PHP_VERSION, V3::INFO ) );
		$arrCont[] = array( 'V3 Version', pakeColor::colorize( V3::BOT_VERSION, V3::INFO ) );
		$arrCont[] = array( 'V3 Stable release', V3::IS_STABLE ? pakeColor::colorize( 'yes', V3::INFO ) : pakeColor::colorize( 'no', V3::ERROR ) );
		$arrCont[] = array( 'PHP Socket support', is_callable( 'socket_create' ) ? pakeColor::colorize( 'yes', V3::INFO ) : pakeColor::colorize( 'no', V3::ERROR ) );
		$strMods = '';
		foreach( new DirectoryIterator( V3_DIR . '/modules' ) as $objFile )
		{
			if( $objFile -> isDot() or strpos( $objFile -> getFilename(), '.php' ) === false )
			{
				continue;
			}
			$strMods .= str_replace( array( 'V3_', '.module.php' ), '', $objFile -> getFilename() ) . ' ';
		}
		$arrCont[] = array( 'Installed modules', pakeColor::colorize( $strMods, V3::INFO ) );
		$arrCont[] = array( 'Is data/ directory writable', is_writable( 'data' ) ? pakeColor::colorize( 'yes', V3::INFO ) : pakeColor::colorize( 'no', V3::ERROR ) );
		$arrCont[] = array( 'Is logs/ directory writable', is_writable( 'logs' ) ? pakeColor::colorize( 'yes', V3::INFO ) : pakeColor::colorize( 'no', V3::ERROR ) );
		;
		
		$arrCont[] = array( 'Eaten cats', pakeColor::colorize( rand( 666, 1337 ), V3::WARNING ) );
		echo $this -> getModule( 'CLI' ) -> renderTable( array( 'Field', 'Value' ), $arrCont );
	}
}
?>

<?php

class V3_ConfigureTask extends V3_Task
{
	/* _init must return true if everything is fine  */

	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '1.0';
		$this -> required_params = array(
		);

		$this -> description = 'Generates configuration file for new bot';

		return true;
	}

	public function _exec()
	{
		$this -> getCore( ) -> initialize( false );
		V3::castEvent( 'idle' );

		V3::log( 'This is V3 New Configuration Wizard. You are needed to supply');
		V3::log( 'minimum amount of data in order to get V3 up and running.');
		V3::log( 'Do not select dependencies in module list as V3 does that for you');
		V3::log( 'and automatically selects required modules for you.');
		V3::log( 'Also note that the Debug module is always loaded.');

		V3::log( '');
		V3::log( 'This Wizard creates BOTNAME.conf file in config/ directory');
		V3::log( 'populating it with some basic bootstrap data');

		$this -> getModule('CLI')->askYesNo('Do you want to continue?') or die();

		$strName = $this -> getModule( 'CLI') -> waitForInput( 'Enter new bot name' );

		while( is_readable( V3_DIR.'/config/'. $strName.'.conf' ) )
		{
			V3::log( $strName.' is already configured, pick different name.' );
			$strName = $this -> getModule( 'CLI') -> waitForInput( 'Enter new bot name' );
		}

		$arrModules = array();
		$arrOptions = array();
		$arrDeps = array();
		foreach( glob( V3_DIR.'/modules/*.module.php' ) as $fileName )
		{
			include_once $fileName;
			$strModName = str_replace( array( V3_DIR, '/modules/', '.module.php' ) ,'', $fileName ). 'Module';

			$strHumanName = str_replace( array( 'V3_', 'Module' ), '', $strModName );
			if( in_array( $strHumanName, array(
				'Arc4', 'Debug', 'CLI', 'Config', 'Socket' ) /* built-in modules, can not disable */
				) )
			{
				continue;
			}
			$objModule = new $strModName();
			$objModule -> _init();

			$arrDeps[$strHumanName] = $objModule->deps;
			$arrModules[] = $strHumanName;
			$arrOptions[$strHumanName] = (array)$objModule->default_config;

			V3::log( sprintf( '[%s] %s', str_pad(sizeof( $arrModules ), 2, '0', STR_PAD_LEFT), $strHumanName, V3::INFO ) );
		}

		V3::log( 'Now please select modules you want to use. Enter numbers separated by commas.');

		$arrMods = explode( ',', trim( $this -> getModule( 'CLI' ) -> waitForInput( ) ) );

		if( empty( $arrMods ) )
		{
			V3::log( 'No modules selected, configuration aborted.' );
			return;
		}

		$this -> getModule( 'CLI' ) -> setArgument( 'name', $strName );

		$this -> getCore( ) -> registerModule( 'Config', array(
            'blnCreateNew' => true )
		);


		$strConf = '[App]'. PHP_EOL;

		$arrSelected[] = 'Debug';


		foreach( array_unique($arrMods) as $intMod )
		{
			$intMod = intval($intMod);
			if( !isset( $arrModules[$intMod-1]))
			{
				V3::log( 'No such module: '. $intMod.'. Configuration aborted.');
				return;
			}

			if( $arrModules[$intMod-1] == 'Debug' )
			{
				continue;
			}

			$arrSelected[] = $arrModules[$intMod-1];
			foreach($arrDeps[$arrModules[$intMod-1]] as $strDep )
			{
				if( $strDep == null OR in_array($strDep,$arrSelected) )
				{
					continue;
				}

				$arrSelected[] = $strDep;
				$strConf .= 'modules[] = '.  $strDep.PHP_EOL;
			}

			$strConf .= 'modules[] = '. $arrModules[$intMod-1].PHP_EOL;
			V3::log( 'Configuring module: '. $arrModules[$intMod-1] );
		}

		$strConf .= 'modules[] = Debug'.PHP_EOL;


		$strConf .= PHP_EOL;

		$blnDefault = $this -> getModule('CLI') -> askYesNo( 'Do you want to use default values for module configuration?');

		foreach( $arrSelected as $strModule )
		{
			if( !empty( $arrOptions[$strModule]))
			{
				$strConf .= '['.$strModule.']'.PHP_EOL;
				foreach($arrOptions[$strModule] as $strOption => $mixValue )
				{
					$strConf .= $strOption.' = ';

					if( !$blnDefault )
					{
						$mixOrig = $mixValue;
						$mixValue = $this -> getModule( 'CLI' ) -> waitForInput( 'Value for '.$strModule.'.'.$strOption.' [ENTER for '.$mixOrig.']');
						if( empty($mixValue ) )
						{
							$mixValue = $mixOrig;
						}
					}

					switch( gettype($mixValue) )
					{
						case 'integer':
							$strConf = $mixValue;
							break;
						case 'string':
							$strConf = '"'.$mixValue.'"';
							break;
						case 'boolean':
							$strConf = $mixValue ? 'true' : 'false';
							break;
						default:
							V3::log('Unknown configuration type: '. gettype($mixValue));
							exit;
							break;
					}

					$strConf .= PHP_EOL;
				}

				$strConf .= PHP_EOL;
			}
		}

		$this -> getModule('Config') -> writeConfig( $strConf );

		V3::log(
			sprintf( 'Ok, that is all we need to know, you can now start your bot with ./V3 start --name %s', $strName
			)
		);

		$blnRun = $this -> getModule( 'CLI' ) -> askYesNo( 'Do you want to run your bot now?');
		if( $blnRun )
		{
			$this -> getCore() -> executeCommand( 'start' );
		}

		V3::log( 'V3 v.' . V3::BOT_VERSION . ' shutting down.' );
	}

}
?>

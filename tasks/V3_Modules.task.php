<?php

class V3_ModulesTask extends V3_Task
{
    /* _init must return true if everything is fine */

    public function _init()
    {
        $this -> author = 'xardas';
        $this -> version = '0.0.5';
        $this -> required_params = array(
        );

        $this -> description = 'Prints available modules to use';

        return true;
    }

    public function _exec()
    {
        $this -> getCore( ) -> initialize( false );


		$arrHeaders = array( 'Module', 'Description',  'Author', 'Version', 'Compatibility', 'Dependencies' );

		$arrData = array();

		foreach( glob( V3_DIR.'/modules/*.module.php' ) as $fileName )
		{
			include_once $fileName;
			$strModName = str_replace( array( V3_DIR, '/modules/', '.module.php' ) ,'', $fileName ). 'Module';

			$strHumanName = str_replace( array( 'V3_', 'Module' ), '', $strModName );
			$objModule = new $strModName();
			$objModule -> _init();

			$strDeps = str_pad( implode( ', ', $objModule->deps), 14, ' ', STR_PAD_RIGHT );
			$strCompat = implode( ' - ', $objModule->compat).'  ';

			$arrData[] = array( $strHumanName,
					$objModule->description,
					str_pad( $objModule->author, 8, ' ', STR_PAD_RIGHT ),
					str_pad( $objModule->version, 10, ' ', STR_PAD_RIGHT ),
					$strCompat,
					$strDeps );
		}


		echo $this -> getCore() -> getModule( 'CLI' ) -> renderTable( $arrHeaders, $arrData );
    }

}
?>

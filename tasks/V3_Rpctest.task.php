<?php
class V3_RpcTestTask extends V3_Task
{

	/* _init must return true if everything is fine */
	public function _init()
	{
		$this -> author = 'xardas';
		$this -> version = '0.0.1';
		$this -> required_params = array();
		$this -> description = 'Tests V3_RPCCommandChain object';
		return true;
	}

	public function _exec()
	{
		$n =$this ->getModule('CLI')->waitForInput('Enter your name' );
		V3::log( 'Hi '.$n );
		V3::log( 'Creating V3_RPCCommandChain instance' );
		//V3_Loader::import( 'core.RPCCommandChain' );
		$objRPC = new V3_RPCCommandChain( 'test' );
		V3::log( 'Adding getCore to stack' );
		$objRPC -> add( 'getCore' );
		V3::log( 'Adding terminate to stack' );
		$objRPC -> add( 'terminate', array( 'because i got high' ) );
		V3::log( 'Testing serialize/unserlialize' );
		$strRPC = serialize( $objRPC );
		$objRPC = unserialize( $strRPC );
		V3::log( 'Executing chain' );
		$mixResult = $objRPC -> execute();
	}
}
?>

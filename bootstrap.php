<?php
/**
 * V3 bootstrap file - gets V3 platform ready to play with
 * 
 * @author xardas
 * @package V3
 * @subpackage Boot
 */

/*
 *  Will be managed by V3_Exception and configuration anyway 
 *  test.
 *  */
error_reporting(E_ALL|E_STRICT);
ini_set( 'display_errors', 1 );

define( 'V3_DIR', dirname( __FILE__) );
date_default_timezone_set( 'Europe/Warsaw' );
require_once V3_DIR.'/classes/core/V3_Accessors.class.php';
require_once V3_DIR.'/classes/core/V3_Loader.class.php';
require_once V3_DIR.'/classes/core/V3_Exception.class.php';

set_time_limit( 0 );
declare( ticks=1 ); // apparently pcntl_signal needs ticks to work

function sig_handler()
{
        echo V3::CRLF;
        V3::terminate( 'SIGINT (Ctrl+C) Signal caught' );
        return true;
}

if( function_exists( 'pcntl_signal' ) )
{
	pcntl_signal( SIGINT, 'sig_handler' );
}

V3_Exception::bindHandlers(); /* from now on any errors will be taken care of ;-) */

V3_Loader::initialize( V3_DIR );
V3_Loader::setDebug( false );
V3_Loader::import( 'classes.V3' );

V3::defineBaseConstants();
V3::importBaseClasses();

$objCore = new V3_Core();
V3::registerCore( $objCore );

$objCore -> registerModule( 'CLI' );
$objCore -> initialize(false);

/**
 * V3 ready here, without main loop tho'
 */
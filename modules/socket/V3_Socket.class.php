<?php
/**
 * 
 * Represents single V3 internet socket. Multi-purpose - it represents listening, connect and client sockets.
 * Has many convenience methods that elliminate the need for using socket_* functions or any other functions.
 * 
 * @author xardas
 * @package V3
 * @subpackage Socket
 */
class V3_Socket extends V3_Accessors
{
	private $arrQueues = array();
	private $intLastRead = 0;
	private $intLastWrite = 0;


	public function getLastReadTime()
	{
		return $this -> intLastRead;
	}
	
	public function getLastWriteTime()
	{
		return $this -> intLastWrite;
	}
	
	public function registerQueue( $strName, $intInterval = 0 )
	{
		$objI = new V3_Interval();
		$objI -> setInterval( $intInterval );
		$objI -> setCallableByRef( $this, 'doQueue' );
		$objI -> setArgs( array( $strName ) );

		$this -> addInterval( 'queue_'. $strName, $objI );
		$this -> arrQueues[ $strName ] = array('interval'=>$intInterval,'data'=>array(),'i_object'=>$objI);
	}

	public function doQueue( $strQueue, V3_Interval $objI )
	{
		//V3::log( 'Scheduled queue pop: '. $strQueue, V3::VERBOSE );
		if( sizeof( $this -> arrQueues[ $strQueue ]['data'] ) > 0 )
		{
			$this -> write( array_shift( $this -> arrQueues[ $strQueue ]['data'] ), false, ''  );
		}
		else
		{
			// Why should doQueue be called when queue is empty? Let's suspend it..
			$objI -> suspend();
		}
		
		return true;
	}

	public function __construct( $arrParams )
	{
		$this -> registerProperties( $arrParams );
	}

	public function write( $strData, $blnAppendCrlf = true, $strQueue = '' )
	{
		// Todo: Queue support - DONE
		
		
		
		$this -> intLastWrite = time();
		if( $blnAppendCrlf )
		{
			$strData .= V3::CRLF;
		}

		if( $strQueue != '' )
		{
			if( isset( $this -> arrQueues[ $strQueue ] ) )
			{
				if( $this -> arrQueues[$strQueue]['interval'] > 0 )
				{
					array_push( $this -> arrQueues[ $strQueue ]['data'], $strData );

					// Unsuspend queue if some data came...
					$this -> arrQueues[ $strQueue ]['i_object'] -> unsuspend();
				}
				else
				{
					return socket_write( $this -> socket, $strData );
				}
				return true;
			}
			else
			{
				throw new Exception( sprintf( 'Trying to send message with non-registered queue: %s', $strQueue ) );
			}
		}

		return socket_write( $this -> socket, $strData );

	}

	public function read( $blnRealRead = false)
	{
		$this -> intLastRead = time();
		if( $blnRealRead )
		{
			$strRead = trim( socket_read( $this -> socket, 65536 ) );
		}
		else
		{
			$strRead = $this -> read_data;
			$this -> read_data = '';
		}
		return !empty( $strRead ) ? $strRead : false;
	}
	
	public function waitTillClosed()
	{
			while( $this -> read( true ) )
			{
//			@socket_close( $this -> socket );
				usleep( 50000 );
			}
	}

	public function close( $blnWaitToFinish = false)
	{
		V3::log( sprintf( 'Closing socket: type=%s host=%s port=%s module=%s', $this -> type, $this -> host, $this -> port, $this -> module ), V3::VERBOSE );
		
		if( defined( 'MSG_EOF' ) )
		{
			socket_send( $this->socket, null, 0,  MSG_EOF );
		}
		socket_shutdown( $this -> socket, 2 );
		usleep( 50000 );
		
		if( $blnWaitToFinish )
		{
			$this -> waitTillClosed();
		}
		else
		{
			socket_close( $this->socket );
		}
		
		$this -> notify( $this -> module, 'socket_closed', array( 'socket' => $this ) );
	}
}

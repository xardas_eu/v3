<?php
/**
 * 
 * V3 channel list for Bot. Operates under simple assumption - channel joined is channel added, 
 * the same goes for parting.
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_ChanlistModule extends V3_Module
{
	private $arrChans;
	
	public function _init()
	{
		
		$this -> default_config = array(
			'arc4_key' => 'secret-key-CHANGE-IT',
			'file'	   => '%v3_root%/data/%botname%.chans'
		);
		
		$this -> author  = 'xardas';
		$this -> version = '0.1.0';
		$this -> description = 'Stores channel list for IRC';
		$this -> deps    = array( null );
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			'post_activate' => true,
			'deactivate'	=> true
		);
		return true;
	}
	
	public function addChan( $strChan, V3_Channel $objChan )
	{
		$objChannel = clone $objChan;
		$objChannel -> clearForSave();
		$this -> arrChans[ V3::normalize( $strChan ) ] = $objChannel;
		$this -> saveResource( $this -> arrChans );
	}
	
	public function delChan( $mixChan )
	{
		if( $mixChan instanceof V3_Channel )
		{
			$mixChan = $mixChan -> name;
		} 
		
		$mixChan = V3::normalize( $mixChan );
		
		if( isset( $this -> arrChans[ $mixChan ]))
		{
			unset( $this -> arrChans[ $mixChan ] );
			$this -> saveResource( $this -> arrChans );
			return true;
		}
		
		return false;
		
	}
	
	public function getChannels()
	{
		return $this -> arrChans;
	}
	
	public function event_activate( V3_Event $e )
	{
	
	}
	
	public function handle_post_activate( V3_Event $e )
	{
		$this -> arrChans = $this -> loadResource( $this -> getModuleConf( 'file' ) );
	
		// Saving chanlist every 5 minutes
		$this -> runEvery( 'Chanlist', 'handle_deactivate', 300 );
	}
	
	public function handle_deactivate()
	{
		foreach( $this -> arrChans as $objChan )
		{
			$objChan -> clearForSave();
			
		}
		$this -> saveResource( $this -> arrChans );
	}
}

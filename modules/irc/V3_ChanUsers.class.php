<?php
/**
 *
 * Collection of channel users for every channel
 *
 * @author xardas
 * @see V3_Channel
 * @see V3_ChanUser
 * @package V3
 * @subpackage IRC
 */
class V3_ChanUsers extends V3_Accessors
{
	private $arrUsers = array();

	private function matchMask( $strMask, $strPattern, $intExact = false )
	{
		/*
		 * Oczywiscie wszystkie porownania case-insensitive
		 */

		$arrM = V3::explodeMask($strMask);
		$arrP = V3::explodeMask($strPattern);

		if( !$arrM  )
		{
			$strMask = $strMask.'!*@*';
		}

		if( !$arrP )
		{
			$strPattern = $strPattern.'!*@*';
		}

		if( $intExact == true )
		{
			$strMask = strtolower( $strMask );
			$strPattern = strtolower( $strPattern );

			if( $strMask == $strPattern )
			{
				return 1;
			}
			return 0;
		}

		$arrRepFrom = array( '.', '?', '*', '{', '}', '[', ']', '^' );
		$arrRepTo =   array( '\.', '.', '.*', '\{', '\}', '\[', '\]', '\^' );

		$blnResult = preg_match( '/^'. str_replace( $arrRepFrom, $arrRepTo, $strPattern ). '$/i', $strMask );

		unset( $arrP, $arrM, $strMask, $strPattern, $intExact, $arrRepFrom, $arrRepTo );
		return $blnResult;
	}
	
	public function filter( $strMask )
	{
		$object = clone $this;
		foreach( $object -> arrUsers as $strNick => $objData )
		{
			if( !$this->matchMask( $objData -> getMask(), $strMask, false ) )
			{
				unset( $object -> arrUsers[$strNick] );
			}
		}
		
		return $object;
	}

	public function __construct( $arrParams )
	{
		$this -> registerProperties( $arrParams );
	}

	public function getUser( $strNick )
	{
		return isset( $this -> arrUsers[$strNick]) ? $this -> arrUsers[$strNick] : false;
	}

	public function changeNick( $strOldNick, $strNewNick )
	{
		if( isset( $this -> arrUsers[$strOldNick] ) )
		{
			$this -> arrUsers[ $strNewNick ] = $this -> arrUsers[$strOldNick];
			$this -> arrUsers[ $strNewNick ] -> nick = $strNewNick;
			unset( $this -> arrUsers[ $strOldNick ] );
			return true;
		}

		return false;
	}

	/* add or update */
	public function add( array $arrUser )
	{
		/*
			Array
			(
			[0] => ~v3
			[1] => chello089079064181.chello.pl
			[2] => *.OGameNet.net
			[3] => V3
			[4] => H@x
			)
			*/

		$this -> arrUsers[ $arrUser[3] ] = new V3_ChanUser( array(
			'ident' => $arrUser[0],
			'host'	=> $arrUser[1],
			'server'=> $arrUser[2],
			'nick'	=> $arrUser[3],
			'modes' => preg_replace( '/[^\@\+]/', '', $arrUser[4] )

		) );

		return $this -> arrUsers[$arrUser[3]];
	}

	public function remove( $strNick )
	{
		$arrMask = V3::explodeMask( $strNick );
		if( is_array( $arrMask ) )
		{
			$strNick = $arrMask['nick'];
		}

		if( isset( $this -> arrUsers[$strNick ] ) )
		{
			unset( $this -> arrUsers[$strNick] );
			return true;
		}
		return false;
	}
}
?>

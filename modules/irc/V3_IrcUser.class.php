<?php
/**
 * 
 * Represents single IRC user
 * 
 * @author xardas
 * @package V3
 * @subpackage IRC
 */
class V3_IrcUser extends V3_Accessors
{
	private $chans;
	
	public function __construct( $arrParams = array() )
	{
		$this -> registerProperties( $arrParams );
		
		if( isset($arrParams['chans'])) 
		{
			$this -> chans = $arrParams['chans'];
		}
	}
	
	public function getChans()
	{
		return $this->chans;
	}
	
	public function addChan( V3_Channel $objChan )
	{
		$this -> chans[ V3::normalize( $objChan -> name ) ] = $objChan;
	}
	
	public function delChan( $mixChan )
	{
		if( $mixChan instanceof V3_Channel )
		{
			$mixChan = V3::normalize( $mixChan -> name );		
		}
		
		if( isset($this->chans[$mixChan]) )
		{
			unset($this->chans[$mixChan]);
			return true;
		} 
		
		return false;
	}
}
?>

<?php
/**
 * 
 * Represents single channel user
 * 
 * @author xardas
 * @package V3
 * @subpackage IRC
 */
class V3_ChanUser extends V3_Accessors
{
	public function __construct( $arrParams = array() )
	{
		$this -> registerProperties( $arrParams );
	}
	
	public function getMask()
	{
		return $this -> nick.'!'. $this -> ident.'@'. $this -> host;
	}
	
	public function setMode( $strString )
	{
		
		$strRep = str_replace( array( 'o', 'v'), array( '@', '+' ), $strString[1] );
		if( $strString[0] == '-' )
		{
			$this -> modes = str_replace( $strRep, '', $this -> modes );
		}
		elseif( $strString[0] == '+' && strstr( $this -> modes, $strRep ) === false )
		{
			$this -> modes .= $strRep;
			return true;
		}
	
	}
	
	public function isOp()
	{
		return strstr( $this -> modes, '@') !== false;
	}
	
	public function isVoice()
	{
		return strstr( $this -> modes, '+') !== false;
	}
}
?>

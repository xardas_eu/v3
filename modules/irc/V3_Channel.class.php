<?php
/**
 *
 * Class that represents a joined channel. Or a chanlist channel.
 * If it's the latter - channel-specific settings will be saved by custom properties
 * @author xardas
 * @package V3
 * @subpackage IRC
 *
 */
class V3_Channel extends V3_Accessors
{
	private $arrModes = array();
	private $arrBans  = array();
	private $objUsers;
	private $blnSynced = false;
	private $blnJoined = false;
	
	
	public function setJoined( $blnJ = false )
	{
		$this -> blnJoined = false;
	}


	public function isJoined()
	{
		return $this -> blnJoined;
	}

	public function setSynced( $blnS = false )
	{
		$this -> blnSynced = $blnS;
	}

	public function isSynced()
	{
		$arrN = $this -> getUserNicks();
		return ( ! empty( $arrN ) );
	}

	public function setMode( $strMode, $strValue = null )
	{

		$strRealMode = substr( $strMode, 1 );
		if( $strMode[0] == '-' )
		{
			if( isset( $this -> arrModes[$strRealMode]))
			{
				unset( $this -> arrModes[$strRealMode]);
			}
		}
		else
		{
			$this -> arrModes[ $strRealMode ] = ($strValue) ? $strValue : true;
		}
	}

	public function __construct( $arrParams )
	{
		$this -> registerProperties( $arrParams );
		$this -> objUsers = new V3_ChanUsers( array() );
	}

	public function getBans()
	{
		return $this -> arrBans;
	}

	public function clearForSave()
	{
		$this -> objUsers = array();
		$this -> setTopic( null, false);
		$this -> arrBans = array();
		$this -> arrModes = array();
	}

	public function addBan( array $arrMasks, $blnRealBan = false )
	{
		foreach( $arrMasks as $strMask )
		{
			if( array_search( $strMask, $this -> arrBans ) === false )
			{
				$this -> arrBans[] = $strMask;
			}
		}

		if( $blnRealBan )
		{
			if( $this -> getIrc() -> isOp( $this ) )
			{
					
				$this -> getIrc() -> getIRCSocket() -> write(
				sprintf( 'MODE %s +%s %s', $this -> name, str_repeat( 'b', sizeof( $arrMasks ) ), implode( ' ', $arrMasks) ), true, 'modes'
				
				);
			}
		}
	}

	public function delBan( array $arrMasks, $blnRealUnban = false )
	{
		foreach( $arrMasks as $strMask )
		{
			if( ($intIdx = array_search( $strMask, $this -> arrBans ) ) !== false )
			{
				unset( $this -> arrBans[$intIdx] );
			}
		}

		if( $blnRealUnban )
		{
			if( $this -> getIrc() -> isOp( $this ) )
			{
				$this -> getIrc() -> getIRCSocket() -> write(
				sprintf( 'MODE %s -%s %s', $this -> name, str_repeat( 'b', sizeof( $arrMasks ) ), implode( ' ', $arrMasks) ), true, 'modes'
				
				);
			}
		}
	}

	public function addUser( array $arrUserData )
	{
		return $this -> objUsers -> add( $arrUserData );
	}

	public function delUser( $strNick )
	{
		$this -> objUsers -> remove( $strNick );
	}

	public function getUsers()
	{
		return $this -> objUsers;
	}

	public function setModes( array $arrModes, $blnRealSet = false, $strMode = '+' )
	{
		$this -> arrModes = $arrModes;
		if( $blnRealSet )
		{
			if( $this -> getIrc() -> isOp( $this ) )
			{
				$this -> getIrc() -> getIRCSocket() -> write(
				sprintf( 'MODE %s %s%s', $this -> name, $strMode, implode( '', $arrModes ) ), true, 'modes' );
			}
		}
	}

	public function getModes()
	{
		return $this -> arrModes;
	}

	public function setTopic( $strTopic, $blnRealSet = false )
	{
		$this -> topic = $strTopic;
		if( $blnRealSet )
		{
			if( $this -> getIrc() -> isOp( $this ) )
			{
				$this -> getIrc() -> getIRCSocket() -> write( 'TOPIC '. $this -> name.' :'. $strTopic, true, 'sync' );
			}
		}
	}

	public function getTopic()
	{
		return $this -> topic;
	}
}
?>

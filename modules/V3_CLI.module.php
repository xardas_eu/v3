<?php
/**
 * 
 * Provides support for CLI environment (probably the only env. V3 will ever be used in)
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_CLIModule extends V3_Module
{

    private $arrConfig = array( );

    public function _init()
    {
        $this -> author = 'xardas';
        $this -> version = '0.9';

		$this -> description = 'Provides Command-Line interface';
		
        $this -> deps = array( null );
        $this -> compat  = array( '0.1.0', '0.2.0' );
        $this -> events = array(
        );
        return true;
    }

    private $arrArgs = array( );
    private $strCommand = '';
	
	/**
	 * The original, unshifted argv array
	 * @var array
	 */
	private $arrARGV = array();


	public function renderTable( array $arrHeaders, array $arrData )
	{
		$strTable = '';

		$arrColumnLengths = array();
		$intLongestColumn = 0;
		foreach( $arrHeaders as $intHeader => $strHeader )
		{
			$arrColumnLengths[ $intHeader ] = strlen( $strHeader );
			if( $arrColumnLengths[ $intHeader ] > max( $arrColumnLengths ) )
			{
				$intLongestColumn = $intHeader;
			}
       	}

		foreach( $arrData as $arrRow )
		{
			foreach( $arrRow as $intHeader => $strData )
			{
				$intLen = strlen( $strData );

				if( $intLen > $arrColumnLengths[ $intHeader ] )
				{
					$arrColumnLengths[ $intHeader ] = $intLen;
					$intLongestColumn = $intHeader;
				}
			}
		}

	


		$strFieldSeparator = '|';
		$strRowSeparator   = '-';
		$strTableEdgeChar	= '|';
		$strPaddingChar =	' '; // yeah, just space
		$intPaddingSpaces = 1; // left AND right padding
		$intRowLength = array_sum( $arrColumnLengths ) 
						+ ( strlen( $strFieldSeparator ) * sizeof($arrColumnLengths)   )
						+ $intPaddingSpaces*2*sizeof($arrColumnLengths); // TOTAL maximum row length
		
		$strPadding = str_repeat( $strPaddingChar, $intPaddingSpaces);
	
		$strTable .= str_repeat( $strRowSeparator, $intRowLength ). $strTableEdgeChar. PHP_EOL;
		$strTable .= $strTableEdgeChar;

		$intCharsLeft = $intRowLength - ( strlen( $strTableEdgeChar ) * 2 );
      	foreach( $arrHeaders as $intHeader => $strHeader )
		{
			$intHeaderSize = $arrColumnLengths[ $intHeader ];
			$strHead = $strPadding. $strHeader. $strPadding;
			$intHeadSize = strlen( $strHead );


			$strHead = $strPadding. str_pad( $strHead , $intHeaderSize , ' ', STR_PAD_BOTH). $strPadding;

		
			$strTable .=  $strHead.$strFieldSeparator;
		}

		$strTable .= PHP_EOL. str_repeat( $strRowSeparator, $intRowLength ). $strTableEdgeChar. PHP_EOL;

		/* Duh :-/ */
		foreach( $arrData as $intRow =>  $arrRow )
		{
			$strRow = $strTableEdgeChar;
			foreach( $arrRow as $intHeader => $strData )
			{
				$intHeaderSize = $arrColumnLengths[ $intHeader ];
				$intPadSize = $intHeaderSize;
				
				$strRow .= 
					$strPadding.
					str_pad( trim($strData), $intPadSize, ' ', STR_PAD_RIGHT ).
					$strPadding.
					$strFieldSeparator;
			}

			//$strRow .= $strTableEdgeChar;

			$strTable .= $strRow.PHP_EOL. str_repeat( $strRowSeparator, $intRowLength ). $strTableEdgeChar. PHP_EOL;
		}

      	
		return $strTable;
	}

    public function getCommand()
    {
        return $this -> strCommand;
    }

    public function hasArgument($strArg )
    {
        return isset( $this -> arrArgs[$strArg] );
    }

    public function setArgument($strArg, $strValue )
    {
        $this -> arrArgs[$strArg] = $strValue;
    }

    public function getArgument($strArg )
    {
        if ( isset( $this -> arrArgs[$strArg] ) )
        {
            return $this -> arrArgs[$strArg];
        }
        else
        {
            V3::log( sprintf( 'V3_Cliparser: Unknown argument: "%s"', $strArg ), V3::WARNING );
            return false;
        }
    }

    public function getArguments()
    {
        return $this -> arrArgs;
    }

    public static function waitForInput($strPrompt = '' )
    {
        stream_set_blocking( STDIN, TRUE );

        echo V3::CRLF . pakeColor::colorize( $strPrompt.' >> ', V3::INFO );
        $strReturn = trim( fread( STDIN, 1024 ) );
        stream_set_blocking( STDIN, FALSE );
        return $strReturn;
    }

    public static function askYesNo($strPrompt = 'Y/N' )
    {

        if ( $strPrompt != 'Y/N' )
        {
            $strPrompt .= ' [Y/N]';
        }

        $arrAccepts = array(
            array( 'Tak', 'T', 't', 'Yes', 'Yeah', 'Yeah!', 'Y', 'y', 'yes', 'yea', 'yeah', 'true', 'Woo-hoo!' ), // positive
            array( 'Nie', 'N', 'n', 'No', 'Damn!', 'N', 'n', 'no', 'hellno', 'not', 'false', "D'oh!" ) // negative
        );

        $strReturn = '';


        while ( true )
        {
            if ( in_array( $strReturn, $arrAccepts[0] ) || in_array( $strReturn, $arrAccepts[1] ) )
            {
                break;
            }

            stream_set_blocking( STDIN, TRUE );

            echo pakeColor::colorize( 
						V3::CRLF . pakeColor::colorize( $strPrompt, V3::INFO ) );
            $strReturn = trim( fread( STDIN, 1024 ) );
            stream_set_blocking( STDIN, FALSE );
        }

        return in_array( $strReturn, $arrAccepts[0] ) ? true : false;
    }

	public function parseArgs( $blnShiftFirst = true )
	{
		$arrArgv = $this -> arrARGV;

		if( $blnShiftFirst )
		{
			$strCmd = array_shift( $arrArgv );
			$this -> strCommand = $strCmd;
		}
        $strArgv = trim( implode( ' ', $arrArgv ) );

        preg_match_all( '/-{1,2}([a-zA-Z0-9\-\._]+)={0,1}\s{0,1}([a-zA-Z0-9\._@\*!]+){0,1}/i', $strArgv, $arrArgs );

        foreach ( $arrArgs[1] as $intArg => $strArg )
        {
            $this -> arrArgs[$strArg] = $arrArgs[2][$intArg];
        }
	}

    public function event_activate(V3_Event $e )
    {
        if ( !isset( $_SERVER['argv'] ) )
        {
            throw new V3_Exception( 'Please use V3 from your CLI environment' );
        }
		
        $arrArgv = $_SERVER['argv'];
		array_shift( $arrArgv );
		$this -> arrARGV = $arrArgv;
        
        if ( empty( $arrArgv ) )
        {
            V3::log( 'No command specified, try "help".', V3::FATAL );
            exit;
        }

        $this -> parseArgs();
    }

}

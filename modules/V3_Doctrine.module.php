<?php
/**
 * V3_DoctrineModule
 * Enables database support in V3
 * 
 * Not that i FORCE people to use Doctrine ORM - there's still getConnection() for pure PDO joy! 
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 *
 */
class V3_DoctrineModule extends V3_Module
{
	private $connection = null;
	private $blnFirstConnect = true ;
	
	public function getConnection()
	{
		return $this->connection;
	}
	
	public function _init()
	{
		$this -> default_config = array(
			'hostname' => 'localhost',
			'username' => 'username',
			'password' => 'password',
			'database' => 'database'
		);
		
		$this -> author  = 'xardas';
		$this -> version = '1.0';
		$this -> description = 'Module that sets up Doctrine connection';
		$this -> deps    = array(null); /* zależności */
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array();
		return true;
	}

	/**
	 * 
	 * Some checks that ensures that during every V3 cycle connection is available.
	 * If it's not - reconnect will happen.
	 * So: no more KPP problems :)
	 * 
	 * @param V3_Interval $i
	 */
	public function checkConnection( V3_Interval $i)
	{
		if( !$this -> getConnection() -> isConnected() )
		{
			$this -> connect();	
		}
	}
	
	public function event_activate( V3_Event $e )
	{
		V3::log( 'Doctrine database support loading..', V3::INFO);
		V3_Loader::import( 'lib.doctrine.Doctrine' );
		spl_autoload_register(array('Doctrine', 'autoload'));

		$this -> persist( 'doctrine_manager', Doctrine_Manager::getInstance() );	
		$this -> connect();
	}
	
	private function connect()
	{
		$arrConfig = $this -> getConfig() -> getModuleConf( 'Doctrine' );
		V3::log('Connecting to database at '.sprintf( 'mysql:dbname=%s;host=%s', $arrConfig['database'], $arrConfig['hostname'] ), V3::VERBOSE);
	
		try
		{
			$dbh = new PDO(sprintf( 'mysql:dbname=%s;host=%s', $arrConfig['database'], $arrConfig['hostname'] ),
				$arrConfig['username'],
				$arrConfig['password'] );
			$this -> connection = Doctrine_Manager::connection($dbh);
			V3::log('Connection to DB established');
			$this -> blnFirstConnect = false;
			$this -> runEvery( 'Doctrine' , 'checkConnection', 15 );
		}
		catch( Exception $e )
		{
			V3::log('Could not establish database connection');
			/**
			 * Doctrine module will NOT register if it cannot connect to DB,
			 * so that modules that depend on it won\'t be run
			 */
			
			if( $this -> blnFirstConnect )
			{
				$this -> getCore() -> unregisterModule( 'Doctrine' );
			}
		}
	}
	

}
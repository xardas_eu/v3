<?php
/**
 * 
 * Some greater playground
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_NSModule extends V3_Module
{
	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.0.1';
		$this -> description = 'Playground for even newer stuff';
		$this -> deps    = array( 'Arc4', 'CLI', 'Doctrine'); /* zależności */
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array();
		return true;
	}
	
	public function socket_accepted( V3_Event $e )
	{
		V3::log('Got client. Gonna do something \'bout it, maybe! SOCKET ALERT!');
		$e -> socket -> times = 1;
	}

	public function socket_read( V3_Event $e )
	{	
		$socket = $e -> socket;
		$data = $socket -> read();
		$sockData=$socket->host.':'.$socket->port;
		V3::log('Our lovely socket '. $sockData.'wrote: '. $data);
		
		if($data=='die')
		{
			return $this->terminate('master ordered me to die so i die');
		}

		$socket->times++;			// testing sockets capability to transfer
		$times = $socket->times;	// arbitrary metadata assigned by server 
		
		$socket -> write( '[times:'. $times.']'. $data);
	}
	
	public function socket_closed( V3_Event $e )
	{
		V3::log('Client got disconnected, crap!');
	}
	
	public function event_activate( V3_Event $e )
	{
		V3::log( 'Testing new mixins capability' );	 
		V3::test( 
			get_class( $this->retrieve('doctrine_manager') ), 
			'==', 
			'Doctrine_Manager', 
			'Testing whether persisted object "manager" is instance of Doctrine_Manager' 
		);
		
		V3_Mixins::register( 'V3_NSModule', 'mixinCall', $this, 'mixinFunction');
		V3_Mixins::register('V3_NSModule', 'getVersion', $this->getCore(), 'handleArgumentVersion');
		 
		$this -> mixinCall( 'test' ); // should execute mixinFunction() with $a = string(4) "test"
		$this -> getVersion(); // should print v3 version
	
		V3::log( 'Memory usage so far: '. (memory_get_usage(true)/1024).' KB');
		$objSocket = $this -> socketBind( '0.0.0.0', 666, V3::SOCKET_TEXT_READ, 51 /* max clients */ );
		V3::log( 'Memory usage 1 socket later: '. (memory_get_usage(true)/1024).' KB');
		
		//$objM = $this -> getCore() -> getSharedMemory(); // needs php compiled with --with-shmop
	}

	public function mixinFunction($a)
	{
		V3::log('called mixin with: '.$a);
	}
}
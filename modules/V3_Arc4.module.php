<?php
/**
 * 
 * Provides strong, symmetric, stream cipher for internal module use.
 * Modules may (and should) use it to encrypt their datafiles (resources) and their internal variables
 * if needed.
 *
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_Arc4Module extends V3_Module
{

    private $arrKeys = array( );

    public function _init()
    {
        $this -> author = 'xardas';
        $this -> version = '1.0';
        $this -> deps = array( null );

        $this -> description = 'Strong stream cipher.. now in V3!';

        $this -> compat  = array( '0.1.0', '0.2.0' );
        $this -> events = array(
            'module_registered' => array( 'event_module_registered' ),
        );
        return true;
    }

    public function event_activate(V3_Event $e )
    {
        
    }

    public function setKey($strModule, $strKey )
    {
        $k = '';
        while ( strlen( $k ) < 256 )
        {
            $k.=$strKey;
        }

        $k = substr( $k, 0, 256 );
        $this -> arrKeys[$strModule] = '';
        for ( $i = 0; $i < 256; $i++ )
        {
            $this -> arrKeys[$strModule] .= chr( $i );
        }
        $j = 0;
        for ( $i = 0; $i < 256; $i++ )
        {
            $t = $this -> arrKeys[$strModule][$i];
            $j = ($j + ord( $t ) + ord( $k[$i] )) % 256;
            $this -> arrKeys[$strModule][$i] = $this -> arrKeys[$strModule][$j];
            $this -> arrKeys[$strModule][$j] = $t;
        }
    }

    public function encryptDecrypt($strModule, $strText )
    {
        if ( !isset( $this -> arrKeys[$strModule] ) )
        {
            V3::log( sprintf( '%s wants to encrypt but didn\'t set its key - returning plaintext', V3::WARNING ) );
            return $strText;
        }

        $len = strlen( $strText );
        $a = 0;
        $b = 0;
        $c = $this -> arrKeys[$strModule];
        $out = '';
        for ( $i = 0; $i < $len; $i++ )
        {
            $a = ($a + 1) % 256;
            $t = $c[$a];
            $b = ($b + ord( $t )) % 256;
            $c[$a] = $c[$b];
            $c[$b] = $t;
            $k = ord( $c[(ord( $c[$a] ) + ord( $c[$b] )) % 256] );
            $out.=chr( ord( $strText[$i] ) ^ $k );
        }
        return $out;
    }

    public function event_module_registered(V3_Event $e )
    {
        if ( $e -> name == 'Arc4' )
        {
            return false;
        }
        $objMod = $this -> getModule( $e -> name );

        $strKey = $this -> getConfig( ) -> getConf( $objMod -> getName( ), 'arc4_key' );
        if ( $strKey )
        {
            V3::log( 'Module ' . $objMod -> getName( ) . ' makes use of arc4 encryption - setting key' );
            $this -> setKey( $objMod -> getName( ), $strKey );
        }
    }

}

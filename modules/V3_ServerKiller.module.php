<?php
/**
 * 
 * Proof-of-concept about V3 socket and CPU performance (excellent ;-))
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_ServerKillerModule extends V3_Module
{
	
	private $arrSockets;
	
	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.0.1';
		$this -> description = 'Tries to kill server from V3_NS';
		$this -> deps    = array( 'Arc4', 'CLI'); /* zależności */
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array();
		return true;
	}

	function socket_read( V3_Event $e )
	{
		$socket = $e -> socket;
		$socket->uses++;
		$socket->write('dupa: '.$socket->uses);
	}
	
	public function socket_closed( V3_Event $e )
	{
		V3::log('Client got disconnected, crap!');
	}
	
	public function event_activate( V3_Event $e )
	{
		V3::log( 'Memory usage so far: '. (memory_get_usage(true)/1024).' KB');	
		$arrConf = $this ->getConfig() -> getModuleConf( 'ServerKiller' );
		
		for( $i = 1; $i <= $arrConf['conn_sockets']; $i++ )
		{
			$objSocket = $this -> socketConnect( $arrConf['host'], $arrConf['port'], V3::SOCKET_TEXT_READ );
			
			if( $objSocket instanceof V3_socket )
			{
				$this->arrSockets[$objSocket->sock_id] = $objSocket;
				V3::log('Opening socket '.$i);
				$objSocket -> write('hello');
				$objSocket -> uses =0 ;
			}
		}
		
		V3::log( 'Memory usage lotsof socket later: '. (memory_get_usage(true)/1024).' KB');
	}
	
}
<?php
/**
 * 
 * V3 user list for Bot.
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_UserlistModule extends V3_Module
{
	private $arrUsers;
	
	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.0.1';
		$this -> deps    = array( null );
		
		
		$this -> default_config = array(
			'arc4_key' => 'secret-key-CHANGE-IT',
			'file'	   => '%v3_root%/data/%botname%.users'
		);

		$this -> description = 'Stores user list for IRC';

		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			'post_activate' => true,
			'deactivate'	=> true
		);
		return true;
	}
	
	public function event_activate( V3_Event $e )
	{
	
	}
	
	public function handle_post_activate( V3_Event $e )
	{
		$this -> arrUsers = $this -> loadResource( $this -> getModuleConf( 'file' ) );
		// Saving userlist every 5 minutes
		$this -> runEvery( 'Userlist', 'handle_deactivate', 300 );
	}
	
	public function handle_deactivate()
	{
		$this -> saveResource();
	}
}

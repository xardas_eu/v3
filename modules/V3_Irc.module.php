<?php
/**
 *
 * V3 IRC backend. Used by bot module. Aims to be 100% accurate with according RFCs.
 *
 * @author xardas
 * @package V3
 * @see V3_BotModule
 * @subpackage Modules
 */
class V3_IrcModule extends V3_Module
{
	private $arrServers = array();
	private $objSocket = null;
	public  $arrServer  = array();
	private $arrServerSettings = array();
	public  $blnRegistered = false;
	private $arrChans  = array();
	public  $intLastReply = 0;
	public  $blnReconnect = true;
	private $blnAway = false;
	private $arrUsers = array();
	private $strMotd = '';

	private $arrJoinQueue = array();


	public static $arrNoParams = array( 'i', 'm', 'n', 't', 'c', 'C', 'd', 'D', 'p', 's', 'r' );

	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.3';
		
		
		$this -> default_config = array(
			'default_port'	 => 6667,
			'nick'			 => '%botname%',
			'alternate_nick' => '%botname%?',
			'keepnick'		 => true,
			'keepnick_interval' => 120,
			'real_name'		 => '%botname%',
			'user_name'		 => '%botname%',
			'servers'		 => '',
			'vhost'			 => '',
			'quit_reason'	 => 'V3 goes down',
			'part_reason'	 => 'V3 goes to hell',
			'onconnect_modes'=> '+i',
			'privmsg_delay'	 => 2,
			'modes_delay'	 => 1,
			'kicks_delay'	 => 0,
			'server_timeout' => 240,
			'reconnect_delay'=> 45
		);

		$this -> description = 'Base of all IRC modules';

		$this -> deps    = array(null);
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			'joined_irc' => array( 'event_joined_irc' ),
			'irc_joined_chan_self' => array( 'syncChan' ),
			'idle'					=> array( 'idle' )
		);
		return true;
	}

	public function __destruct()
	{
		$this -> disconnect();
	}

	/**
	 * Adds channel on the top of the queue to join
	 *
	 * BEWARE OF THIS FUNCTION!!!
	 * Use it ONLY when you are 100% SURE that, scheduledJoins() will be executed, e.g.
	 * an irc_315 (channel syncronized) will appear. So it is pretty much function just to define
	 * loading channel list for startup.
	 * In modules, where immediate join is needed - use joinChan (and rejoin will work just fine)
	 *
	 * @param $objChan
	 */
	public function scheduleJoin( V3_Channel $objChan )
	{
		V3::log('Scheduling to join '. $objChan -> name, V3::INFO );
		$this -> arrJoinQueue[] = $objChan;
	}

	public function scheduledJoins()
	{
		if( empty( $this -> arrJoinQueue ) )
		{
			return;
		}

		$objChan = array_shift( $this -> arrJoinQueue );
		if( $objChan instanceof V3_Channel )
		{
			$this -> joinChan( $objChan );
		}
	}

	public function getServerMOTD()
	{
		return $this -> strMotd;
	}

	public function getIRCSocket()
	{
		return $this -> objSocket;
	}

	public function idle()
	{
		if( $this -> intLastReply > 0 AND time() - $this -> intLastReply >= $this -> intTimeout AND time() - $this -> objSocket -> getLastWriteTime() >= $this -> intTimeout )
		{
			// 4 minutes of silence - can't be!
			if( $this -> isIRCRegistered() )
			{
				V3::log( 'Possibly stoned server, disconnecting', V3::NOTICE );
				$this -> disconnect();
				$this -> runOnce( 'Irc', 'connect', 30 );
			}
		}
	}

	public function isIRCRegistered()
	{
		return $this -> blnRegistered;
	}

	public function getUsers()
	{
		return $this -> arrUsers;
	}

	public function getChans()
	{
		return $this -> arrChans;
	}

	public function isAway()
	{
		/* Returns true if bot has the away flag set on server */
		return $this -> blnAway;
	}

	public function setRegistered( $blnReg = false )
	{
		$this -> blnRegistered = $blnReg;
	}

	public function getCurrentServer()
	{
		return $this -> arrServer[0];
	}

	public function getPort()
	{
		return $this -> arrServer[1];
	}

	public function event_activate()
	{
		foreach( explode( ',', $this -> getModuleConf( 'servers' ) ) as $strServer )
		{
			$this -> arrServers[] = trim( $strServer );
		}

		$this -> intTimeout = $this -> getModuleConf( 'server_timeout', 240 );

		V3_Loader::import( 'modules.irc.*');
	}

	public function reconnect()
	{
		$this -> disconnect( null, false );
		$this -> connect();
	}

	public function connect()
	{
		if( $this -> isIRCRegistered() )
		{
			V3::log( 'Already registered, use reconnect() if you want to force reconnect', V3::NOTICE );
			return true;
		}
		shuffle( $this -> arrServers );
		$this -> arrServer = explode( ':', $this -> arrServers[0] );
		if( !isset( $this -> arrServer[1] ) )
		{
			$this -> arrServer[1] = $this -> getModuleConf( 'default_port', 6667 );
		}
		V3::log( sprintf( 'Connecting to %s at port %s', $this -> arrServer[0], $this -> arrServer[1] ) );

		$objSocket = $this -> socketConnect( $this -> getCurrentServer(), $this -> getPort(), V3::SOCKET_TEXT_READ, $this -> getModuleConf( 'vhost' ) );

		V3::test( is_object( $objSocket ), null, null, 'Testing whether we got a valid V3_Socket connection' );
		if( !$objSocket instanceof V3_Socket )
		{
			V3::log( 'Unable to connect to host, trying again in 60 seconds.', V3::WARNING );
			$this -> runOnce( 'Irc', 'connect', 60 );
		}
		else
		{
			$objSocket -> registerQueue( 'sync', 1 ); // so channel syncing won't trigger server throttle
			$objSocket -> registerQueue( 'privmsg', $this -> getModuleConf( 'privmsg_delay' ) );
			$objSocket -> registerQueue( 'modes', $this -> getModuleConf('modes_delay') );
				
			$objSocket -> registerQueue( 'kicks', $this -> getModuleConf('kicks_delay'));

			$this -> objSocket = $objSocket;
			$this -> register( $objSocket );
		}
	}

	public function disconnect( $strReason = '', $blnReconnect = true, $blnSendQUIT = true )
	{
		if( empty( $strReason ) )
		{
			$strReason = $this -> getModuleConf( 'quit_reason' );
		}

		$this -> blnReconnect = $blnReconnect;

		if( !$this -> objSocket instanceof V3_Socket )
		{
			$this -> notify( 'Irc', 'socket_closed' );
			return false;
		}

		if( $blnSendQUIT )
		{
			$this -> objSocket -> write( sprintf( 'QUIT :%s', $strReason ) );
		}
		$this -> objSocket -> waitTillClosed();
		$this -> setRegistered( false );
		//$this -> objSocket -> close( true );
		sleep( 1 );
	}

	public function privmsg( $strTarget, $strText )
	{
		$arrText = explode( "\n", $strText );
		foreach( $arrText as $strText )
		{
			$this -> objSocket -> write( sprintf( 'PRIVMSG %s :%s ', $strTarget, $strText ), true, 'privmsg' );
		}
	}

	public function setAway( $strAway = '' )
	{
		return $this -> objSocket -> write( 'AWAY :'. $strAway );
	}

	public function register( V3_Socket $objSocket )
	{
		if( isset( $this -> arrServer[2] ) )
		{
			// Server needs password
			$objSocket -> write( sprintf( 'PASS %s', $this -> arrServer[2] ) );
		}


		$objSocket -> write( sprintf( 'USER %s %s %s :%s',
		$this -> getModuleConf( 'user_name'),
		$this -> getModuleConf( 'vhost', 'i.am.voodoo' ),
		$this -> getCurrentServer(),
		$this -> getModuleConf( 'real_name') ) );

		$this -> setNick( $this -> getModuleConf( 'nick' ) );
	}

	public function setNick( $strNick )
	{
		V3::log( 'Setting nickname to '. $strNick, V3::INFO );
		$this -> nick = $strNick;
		$this -> objSocket -> write( sprintf( 'NICK %s', $strNick ) );
	}

	public function getNick()
	{
		return $this -> nick;
	}

	public function whois( $strNick )
	{
		if( ( $arrM = V3::explodeMask( $strNick ) ) !== false )
		{
			$strNick = $arrM[ 'nick' ];
		}
		$this -> objSocket -> write( sprintf( 'WHOIS %s %s', $strNick, $strNick ) );
		$this -> persist( 'whois_nick', $strNick );
		$this -> persist( 'whois_data_' . $strNick, array( 'nick' => $strNick, 'away' => false ) );
		
		
		$objE = $this -> waitForEvent( 'irc_reply_318' );
		if( !$objE instanceof V3_Event )
		{
			// ->breakWait() happened, sth went wrong in the process
			return false; // no such user
		}
		$this -> persist( 'whois_nick', null );

		return $this -> getUser( null, null, $this -> retrieve( 'whois_data_' . $strNick ) );
	}



	public function event_joined_irc( V3_Event $e )
	{
		if( ( $strModes = $this -> getModuleConf( 'onconnect_modes' ) ) !== false )
		{
			$e -> socket -> write( sprintf( 'MODE %s %s', $this -> getNick(), $strModes ), true, 'modes' );
		}


		/* Temp */

	}

	public function socket_closed( V3_Event $e )
	{
		if( $this -> blnReconnect )
		{
			$reconnect_delay = $this -> getConf( 'reconnect_delay', 60 );
			V3::log( 'Disconnected from server, reconnecting in '.$reconnect_delay.' seconds', V3::NOTICE );
			$objInt = new V3_Interval();
			$objInt -> setCallable( 'Irc', 'connect' );
			$objInt -> setInterval( $reconnect_delay );
			$objInt -> setMaxCalls( 1 );
			$this -> addInterval( 'reconnect', $objInt );
		}
		else
		{
			V3::log( 'Disconnected from server.', V3::NOTICE );
		}
	}

	public function getChan( $strChan )
	{
		$strChan = V3::normalize( $strChan );


		if( !isset( $this -> arrChans[ $strChan ] ) )
		{
			$this -> arrChans[$strChan] = new V3_Channel( array( 'name' => '#'. $strChan, 'channel_key' => '' ) );
			$this -> arrChans[$strChan] -> setSynced(false);
		}

		return $this -> arrChans[ $strChan ];
	}

	public function joinedChan( $strChan )
	{
		return isset( $this -> arrChans[ V3::normalize( $strChan ) ] );
	}

	private function changeUserNick( $strOldNick, $strNewNick )
	{
		if( isset( $this -> arrUsers[ $strOldNick ] ) )
		{
			$this -> arrUsers[ $strNewNick ] = $this -> arrUsers[$strOldNick];
			$this -> arrUsers[ $strNewNick ] -> nick = $strNewNick;
			unset( $this -> arrUsers[ $strOldNick ] );
			return true;
		}

		return false;
	}

	public function getUser( $strMask, $strChan = '', array $arrData = null )
	{
		$arrMask = V3::explodeMask( $strMask );
		if( $arrMask === false )
		{
			// maybe it is not a mask but a nickname
			if( isset( $this -> arrUsers[ $strMask ] ) )
			{
				return $this -> arrUsers[ $strMask ];
			}
		}
		if( ! empty( $arrData ) )
		{
			$objUser = new V3_IrcUser( $arrData );
			$this -> arrUsers[ $arrData[ 'nick' ] ] = $objUser;
			return $objUser;
		}
		if( ! isset( $this -> arrUsers[ $arrMask[ 'nick' ] ] ) )
		{
			$objUser = new V3_IrcUser( array( 'realname' => '', 'ident' => $arrMask[ 'user' ], 'nick' => $arrMask[ 'nick' ], 'host' => $arrMask[ 'host' ], 'chans' => array() ) );
			$this -> arrUsers[ $arrMask[ 'nick' ] ] = $objUser;
		}
		return $this -> arrUsers[ $arrMask[ 'nick' ] ];
	}

	public function joinChan( $mixChan, $strKey = null, $strQueue = 'sync' )
	{
		if( $mixChan instanceof V3_Channel )
		{
			$strChan = $mixChan -> name;
			$strKey = $mixChan -> channel_key;
		}

		$strChan = V3::normalize( $strChan );
		return $this -> objSocket -> write( sprintf( 'JOIN #%s %s', $strChan, $strKey ), true, $strQueue );
	}

	public function isOp( V3_Channel $objChan )
	{
		return strstr( $objChan -> getUsers() -> getUser( $this -> getNick() ) -> modes, '@' ) !== false;
	}

	public function isVoice( V3_Channel $objChan )
	{
		return strstr( $objChan -> getUsers() -> getUser( $this -> getNick() ) -> modes, '+' ) !== false;
	}

	public function partChan( $strChan, $strReason = null )
	{
		$strChan = V3::normalize( $strChan );
		if( $strReason == null )
		{
			$strReason = $this -> getModuleConf( 'part_reason' );
		}
		if( $this -> joinedChan( $strChan ) )
		{
			return $this -> objSocket -> write( sprintf( 'PART #%s :%s', $strChan, $strReason ), true, 'sync' );
		}
		return false;
	}

	public function syncChan( V3_Event $e, $blnOnlyModes = false )
	{
		$this -> getChan( $e -> chan -> name ); // If didnt exist in array
		$objChan = $e -> chan;
		$objChan -> setJoined( true );
		$objSock = $e -> socket;

		$objSock -> write( sprintf( 'MODE %s', $objChan -> name ), true, 'sync' );
		if( $blnOnlyModes )
		{
			return true;
		}

		$objSock -> write( sprintf( 'MODE %s b', $objChan -> name ), true, 'sync' );
		$objSock -> write( sprintf( 'WHO %s', $objChan -> name ), true, 'sync' );
	}

	/*
	 * Main function
	 *
	 * not very long, eh? ;-)
	 */
	public function socket_read( V3_Event $e )
	{
		$this -> intLastReply = time();
		$strMsg = $e -> socket -> read();
		V3::log( 'Irc msg: '. $strMsg, V3::VERBOSE );
		$arrRaw = V3::parseRaw( $strMsg );

		$strPrefix  = substr( $arrRaw[1], 1 );

		$strCommand = strtolower( $arrRaw[2] );
		$strTarget  = $arrRaw[3];
		$strParam   = '';
		if( isset( $arrRaw[4]))
		{
			$strParam = $arrRaw[4];
		}

		$this -> registerEventHandler( 'irc_reply_'. $strCommand, 'irc_reply_'. $strCommand ); // JIT
		$this -> castEvent( 'irc_reply_'. $strCommand, array( 'socket' => $e -> socket, 'prefix' => $strPrefix, 'target' => $strTarget, 'param' => $strParam ) );
	}

	public function irc_reply_notice( V3_Event $e )
	{
		V3::log( 'Notice for: '. $e -> target.': '. $e -> param.' (prefixed: '.$e->prefix.')', V3::VERBOSE );
	}

	public function irc_reply_quit( V3_Event $e )
	{
		// Parse it, example: :xar!~screw@trial-noradim1.stogame.net QUIT :Registered
	}

	public function irc_reply_error( V3_Event $e )
	{
		// not good
		$this -> disconnect( null, true, false );
	}

	public function irc_reply_ping( V3_Event $e )
	{
		$e -> socket -> write( sprintf( 'PONG :%s', $e -> target ) );
	}

	public function irc_reply_nick( V3_Event $e )
	{
		// Syntax: :V317!~v3@chello089079064181.chello.pl NICK :V315

		$arrMask = V3::explodeMask( $e -> prefix );
		if( $arrMask['nick'] == $this -> getNick() )
		{
			$this -> setNick( $e -> target );
		}

		$strNewNick = $e -> target;
		$this -> changeUserNick( $arrMask['nick'], $strNewNick );
		foreach( $this -> arrChans as $objChan )
		{
			$objChan -> getUsers() -> changeNick( $arrMask['nick'], $strNewNick );
		}


	}
	public function getAway()
	{
		/* Returns true if bot has the away flag set on server */
		return $this -> blnAway;
	}

	public function irc_reply_join( V3_Event $e )
	{
		// Self join: :V3!~v3@chello089079064181.chello.pl JOIN #v3
		// Sb join:   :xar!~screw@trial-noradim1.stogame.net JOIN #v3
		$arrMask = V3::explodeMask( $e -> prefix );

		$strChan = $e -> target;
		if( $arrMask['nick'] == $this -> getNick() )
		{
			V3::log( 'Bot has joined channel: '. $strChan, V3::VERBOSE );
			$this -> castEvent( 'irc_joined_chan_self', array( 'socket' => $e -> socket, 'chan' => $this -> getChan( $strChan ) ) );
		}
		else
		{
			$this -> castEvent( 'irc_joined_chan', array( 'socket' => $e -> socket, 'chan' => $this -> getChan( $strChan ), 'user' => $this -> getUser( $e -> prefix ) ) );

			$arrChan = array(
			$arrMask['user'],
			$arrMask['host'],
				'',
			$arrMask['nick'],
				''
				);
					
				$objUser = $this -> getChan( $strChan ) -> addUser( $arrChan );
					
				$this -> getUser( $arrMask['full'] ) -> addChan( $this -> getChan( $strChan ) );
		}
	}

	public function irc_reply_topic( V3_Event $e )
	{
		$objUser = $this -> getUser( $e -> prefix );
		$objChan = $this -> getChan( $e -> target );
		$strTopic = $e -> param;

		$objChan -> setTopic( $strTopic );

		$this -> castEvent( 'irc_changed_topic', array( 'chan' => $objChan, 'user' => $objUser ) );
	}

	public function parseModes( $strModes )
	{
		$arrModes = explode( ' ', $strModes );
		$strModes = array_shift( $arrModes );
		$arrReturnModes = array();
		$chrSign = '';
		$intPadding = 0;
		for( $i = 0, $j = strlen( $strModes ); $i < $j; $i ++ )
		{
			switch( $strModes[ $i ] )
			{
				case '+':
					$chrSign = '+';
					$intPadding --;
					break;
				case '-':
					$chrSign = '-';
					$intPadding --;
					break;
				default:
					if( in_array( $strModes[ $i ], self::$arrNoParams ) or ( $strModes[ $i ] == 'l' and $chrSign == '-' ) )
					{
						$intPadding --;
						$arrReturnModes[] = array( 'mode' => $chrSign . $strModes[ $i ], 'param' => '' );
					}
					else
					{
						$arrReturnModes[] = array( 'mode' => $chrSign . $strModes[ $i ], 'param' => $arrModes[ $i + $intPadding ] );
					}
			}
		}
		return $arrReturnModes;
	}



	public function irc_reply_mode( V3_Event $e )
	{
		// IRC mode
		// Syntax for chanmode:  :xar!~screw@trial-noradim1.stogame.net MODE #v3 +cC
		// Syntax for usermode:  :xar!~screw@trial-noradim1.stogame.net MODE #v3 +o V3

		/*
		 * Remember to:
		 * 	- implement channel mode setting via getChan
		 *  - implement V3_ChanUser mode setting via ->modes
		 */

		$source = V3::explodeMask( $e -> prefix );
		if( $source['nick'] == $this -> getNick() && $e -> target == $this -> getNick() )
		{
			/** Server set some modes upon thee */
			$this -> castEvent( 'irc_own_mode', array( 'mode' => $e -> param ) );
			return;
		}

		$arrRealData = explode( ' ', $e -> target, 2 );
		$objChan = $this -> getChan( $arrRealData[0] );
		$arrModes = $this -> parseModes( $arrRealData[1] );

		foreach( $arrModes as $arrMode )
		{
			if( in_array( $arrMode['mode'], array(
					'+o', '-o', '+v', '-v' 
					) ) )
					{
						/** User mode **/
							
						$objChan -> getUsers() -> getUser( $arrMode['param'] ) -> setMode( $arrMode['mode'] );
							
						if( $arrMode['param'] != $this -> getNick() )
						{
							$this -> castEvent( 'irc_user_chan_mode', array(
							'chan' => $objChan,
							'user' => $objChan -> getUsers() -> getUser( $arrMode['param'] ),
							'mode' => $arrMode['mode']
							) );
						}
						else
						{
							$this -> castEvent( 'irc_self_chan_mode', array(
							'chan' => $objChan,
							'mode' => $arrMode['mode']
							) );
						}
					}
					else
					{
						/** Channel mode **/
							
						$objChan -> setMode( $arrMode['mode'], $arrMode['param'] );
						$this -> castEvent( 'irc_chan_mode', array(
						'chan'	=> $objChan,
						'mode'	=> $arrMode
						) );
					}
		}

	}

	public function irc_reply_kick( V3_Event $e )
	{
		// IRC kick
		// Syntax: :xar!~screw@trial-noradim1.stogame.net KICK #v3 V3 :xar
		// Kicking user
		$objUser = $this -> getUser( $e -> prefix );
		$arrTarget = explode( ' ', $e -> target, 2 );
		// Channel
		$objChan = $this -> getChan( $arrTarget[ 0 ] );
		foreach( explode( ',', $arrTarget[ 1 ] ) as $strTarget )
		{
			$strTarget = trim( $strTarget );
			if( $strTarget == $this -> getNick() )
			{
				$this -> castEvent( 'irc_kick_self', array( 'chan' => $objChan, 'user' => $objUser, 'reason' => $e -> param ) );
			}
			else
			{
				$this -> castEvent( 'irc_kick', array( 'chan' => $objChan, 'user' => $objUser, 'target' => $this -> getUser( $strTarget, $objChan -> name ), 'reason' => $e -> param ) );
			}
		}
	}

	public function irc_reply_privmsg( V3_Event $e )
	{
		/**
		 * @todo: CTCP support
		 */
		if( $e -> target == $this -> getNick() )
		{
			$this -> castEvent( 'irc_privmsg', array( 'msg' => $e -> param, 'user' => $this -> getUser( $e -> prefix ) ) );
		}
		else
		{
			$this -> castEvent( 'irc_chanmsg', array( 'msg' => $e -> param, 'chan' => $this -> getChan( $e -> target), 'user' => $this -> getUser( $e -> prefix ) ) );
		}

	}

	public function irc_reply_invite( V3_Event $e )
	{
		// Syntax: :xar!~screw@TenaciousD.user.OGameNet INVITE V3 #v4
	}

	public function irc_reply_part( V3_Event $e )
	{
		// Syntax for others: :xardas!xardas@my.server.name PART #lodowka :dupa
		// Syntax for me: :V3!~v3@my.server.name PART #lodowka :V3 goes to hell...

		$arrMask = V3::explodeMask( $e -> prefix );
		if( $arrMask['nick'] == $this -> getNick() )
		{
			$this -> castEvent( 'irc_parted_chan_self', array( 'chan' => $this -> getChan( $e -> target )));
			unset( $this -> arrChans[ V3::normalize( $e -> target )]);
		}
		else
		{
			// user parted chan
			$this -> getChan( $e -> target ) -> delUser( $arrMask['nick'] );
			$this -> castEvent( 'irc_parted_chan', array( 'user' => $this -> getUser( $e->prefix ), 'chan' => $this -> getChan( $e -> target )));

			$this -> getUser( $e -> prefix ) -> delChan( $this -> getChan( $e -> target ) );
		}
	}

	public function irc_reply_001( V3_Event $e )
	{

	}

	public function irc_reply_002( V3_Event $e )
	{

	}

	public function irc_reply_003( V3_Event $e )
	{

	}

	public function irc_reply_004( V3_Event $e )
	{

	}


	public function irc_reply_005( V3_Event $e )
	{
		/*
		 * Implement it!
		 * Here the server returns its configuration
		 */

		$arrOut = array( 1 => array(), 2 => array() );
		preg_match_all( '/([A-Z]+)=([0-9a-zA-Z\#\!\+\@\,]+)/', $e -> target, $arrOut );

		foreach( $arrOut[1] as $intKey => $strSetting )
		{
			$this -> arrServerSettings[ strtolower( $strSetting ) ] = $arrOut[2][ $intKey ];
		}


		if( !$this -> isIRCRegistered() )
		{
			// checking because server can send 005 more than once
			$this -> setRegistered( true );
			$this -> castEvent( 'joined_irc', array( 'socket' => $e -> socket ) );
		}
	}

	public function irc_reply_020( V3_Event $e )
	{
		// Please wait while we process your connection
	}

	public function irc_reply_042( V3_Event $e )
	{
		$arrUniq = explode( ' ', $e -> target, 2 );
		$strUniq = trim( $arrUniq[1] );
		V3::log( 'My unique ID is: '. $strUniq, V3::INFO );
		$this -> uniq_id = $strUniq;
	}

	public function irc_reply_251( V3_Event $e )
	{

	}

	public function irc_reply_252( V3_Event $e )
	{
		// Opers online
	}

	public function irc_reply_253( V3_Event $e )
	{

	}

	public function irc_reply_254( V3_Event $e )
	{
		// Channels formed
	}

	public function irc_reply_255( V3_Event $e )
	{
		// Server stats
	}

	public function irc_reply_265( V3_Event $e )
	{
		// Server stats part2 - local users
		// Syntax: :krakow.irc.pl 265 V3 2348 3237 :Current local users 2348, max 3237
	}

	public function irc_reply_266( V3_Event $e )
	{
		// Server stats part3 - global users
		// Syntax: :krakow.irc.pl 266 V3 87447 98675 :Current global users 87447, max 98675
	}

	public function irc_reply_301( V3_Event $e )
	{
		// Syntax: :Gameservers.NJ.US.GameSurge.net 301 V330 V330 :V3!
	}

	public function irc_reply_305( V3_Event $e )
	{
		$this -> blnAway = false;
	}

	public function irc_reply_306( V3_Event $e )
	{
		// Marked as being away
		$this -> blnAway = true;
	}

	public function irc_reply_311( V3_Event $e )
	{
		//whois start
		//Syntax: :JaBSnG2.OGameNet.net 311 V3 xar ~screw TenaciousD.user.OGameNet * :Tenacious D
		$strNick = $this ->retrieve( 'whois_nick' );
		$arrD = $this ->retrieve( 'whois_data_' . $strNick );
		$arrD[ 'realname' ] = $e -> param;
		$arrData = explode( ' ', $e -> target );
		array_shift( $arrData );
		$arrD[ 'ident' ] = $arrData[ 1 ];
		$arrD[ 'host' ] = $arrData[ 2 ];
		$this -> persist( 'whois_data_' . $strNick, $arrD );
	}

	public function irc_reply_312( V3_Event $e )
	{
		// whois server
		//Syntax: :noradim.OGameNet.net 312 V3 xar *.OGameNet.net :OGameNet IRC
		$strNick = $this ->retrieve( 'whois_nick' );
		$arrD = $this ->retrieve( 'whois_data_' . $strNick );
		$arrData = explode( ' ', $e -> target );
		array_shift( $arrData );
		$arrD[ 'server' ] = $arrData[ 1 ];
		$arrD[ 'network' ] = $e -> param;
		$this -> persist( 'whois_data_' . $strNick, $arrD );
	}

	public function irc_reply_315( V3_Event $e )
	{
		// End of WHO = channel synced
		$this -> getChan( $e -> target ) -> setSynced( true );
		$this -> castEvent( 'channel_synced', array( 'chan' => $this -> getChan( $e -> target ), 'socket' => $e -> socket ) );

		/** Ready to load NEXT channel */
		$this -> scheduledJoins();
	}

	public function irc_reply_317( V3_Event $e )
	{ //whois idle
		//Syntax: :noradim.OGameNet.net 317 V3 xar 7 1237863374 :seconds idle, signon time
		$strNick = $this -> retrieve( 'whois_nick' );
		$arrD = $this -> retrieve( 'whois_data_' . $strNick );
		$arrData = explode( ' ', $e -> target );
		array_shift( $arrData );
		$arrD[ 'idle' ] = $arrData[ 1 ];
		$arrD[ 'signon' ] = date( 'H:i:s d-m-Y', $arrData[ 2 ] );
		$this -> persist( 'whois_data_' . $strNick, $arrD );
	}

	public function irc_reply_318( V3_Event $e )
	{//end of whois
		//Syntax:  :JaBSnG2.OGameNet.net 318 V3 xar :End of /WHOIS list.
	}

	public function irc_reply_319( V3_Event $e )
	{ //whois chans
		//Syntax: :OGN2.OGameNet.net 319 V3 xar :#noradim @#v3 #4story.pl #bitefight.pl @#polska @#Aurum
		$strNick = $this -> retrieve( 'whois_nick' );
		$arrD = $this -> retrieve( 'whois_data_' . $strNick );
		$arrData = explode( ' ', str_replace( array( '+', '@' ), '', $e -> param ) );
		$arrD[ 'channels' ] = array();
		foreach( $arrData as $strChan )
		{
			$oChan = $this -> getChan( $strChan );
			$arrD[ 'channels' ][ $oChan -> name ] = $oChan;
		}
		$this -> persist( 'whois_data_' . $strNick, $arrD );
	}

	public function irc_reply_324( V3_Event $e )
	{
		// Channel modes [onjoin]
		// Syntax: :OGN2.OGameNet.net 324 V3 #v3 +tnC
		$arrModes = explode( ' ', $e -> target, 3 );
		//print_r($arrModes);
		$strModes = $arrModes[2];
		$objChan = $this -> getChan( $arrModes[1] );

		$arrM = $this -> parseModes($strModes);
		foreach( $arrM as $arrMode )
		{
			$objChan -> setMode( $arrMode['mode'], $arrMode['param'] );
		}
	}

	public function irc_reply_329( V3_Event $e )
	{
		// Chanmodes set on (timestamp)
	}

	public function irc_reply_332( V3_Event $e )
	{
		// Chan topic (onjoin)
		// Syntax: :OGN2.OGameNet.net 332 V313 #v3 :Test

		$arrChan = explode( ' ', $e -> target, 2 );

		$this -> getChan( $arrChan[1] ) -> setTopic( $e -> prefix, false );
	}

	public function irc_reply_333( V3_Event $e )
	{
		// Chantopic set by
		// Syntax: :JaBSnG2.OGameNet.net 333 V35 #v3 xar 1236559680
	}

	public function irc_reply_345( V3_Event $e )
	{
		// somebody invited somebody to somechan
		// Warn: may be ogamenet-specific
		// Syntax: :DevNull.OGameNet.net 345 #v3 lodowka xar :lodowka has been invited by xar
	}

	public function irc_reply_352( V3_Event $e )
	{
		// WHO row
		// Syntax: :noradim.OGameNet.net 352 V3 #v3 ~v3 chello089079064181.chello.pl *.OGameNet.net V3 H :0 V3 Bot
		$arrRow = explode( ' ', $e -> target, 3 );
		$objChan = $this -> getChan( $arrRow[1] );
		$arrRow = explode( ' ', $arrRow[2] );
		$objUser = $objChan -> addUser( $arrRow );

		$this -> getUser( $objUser -> getMask() ) -> addChan( $objChan );
	}

	public function irc_reply_353( V3_Event $e )
	{
		// Should parse it, returns channel nicklist (brief)
		// Syntax: :noradim.OGameNet.net 353 V33 = #v3 :V33 @xar
	}

	public function irc_reply_366( V3_Event $e )
	{
		// End of /names
	}

	public function irc_reply_367( V3_Event $e )
	{
		// Ban list item
		// Syntax: :JaBSnG2.OGameNet.net 367 V3 #Metin2.pl *!*@Voodoo.user.OGameNet ChanServ 1236853142

		$arrBan = explode( ' ', $e -> target );
		$this -> getChan( $arrBan[1] ) -> addBan( array( $arrBan[2] ) );
	}

	public function irc_reply_368( V3_Event $e )
	{
		// End of ban list
	}

	public function irc_reply_375( V3_Event $e )
	{

	}

	public function irc_reply_372( V3_Event $e )
	{
		// MOTD line
		$this -> strMotd .= $e -> prefix.V3::CRLF;
	}

	public function irc_reply_376( V3_Event $e )
	{

	}

	/* Your fakehost is.. */
	public function irc_reply_396( V3_Event $e )
	{
		$arrH = explode( ' ', $e -> target, 2 );
		$strFakehost = $arrH[1];
		$this -> fakehost = $strFakehost;
		V3::log( 'Authenticated to auth service, assigned fakehost: '. $strFakehost, V3::INFO );
	}

	public function irc_reply_401( V3_Event $e )
	{ // no such nickname
		// Syntax: :Clan-Server.AT.EU.GameSurge.net 401 V3 AuthServ@Services.OGameNet.net :No such nick
		$this -> breakWait();
	}

	public function irc_reply_402( V3_Event $e )
	{
		// no such server [with whois nick nick]
		$this -> breakWait();
	}

	public function irc_reply_403( V3_Event $e )
	{
		// no such channel
		// Syntax: :Burstfire.UK.EU.GameSurge.net 403 V3 tester :No such channel
	}

	public function irc_reply_404( V3_Event $e )
	{
		// Can't speak (+m or +b)
		// Syntax: :DevNull.OGameNet.net 404 V3 #v3 :Cannot send to channel
	}

	public function irc_reply_412( V3_Event $e )
	{
		// No text to send
	}

	public function irc_reply_422( V3_Event $e )
	{
		// No MOTD
	}

	/*
	 * Nickname in use
	 */
	public function irc_reply_433( V3_Event $e )
	{
		V3::log( 'My nickname is taken!', V3::WARNING );

		if( !$this -> isIRCRegistered() )
		{
			$strNewNick = str_replace( '?', rand( 1, 30 ), $this -> getModuleConf( 'alternate_nick' ) );
			$e -> socket -> write( 'NICK '. $strNewNick );
			$this -> nick = $strNewNick;
		}
		else
		{
			V3::log( 'But no need to generate nick as I\m registered.', V3::VERBOSE );
		}

		if( $this -> getModuleConf( 'keepnick' ) )
		{
			$objInt = new V3_Interval();
			$objInt -> setInterval( $this->getModuleConf('keepnick_interval') );
			$objInt -> setArgs( array( $this -> getModuleConf('nick') ) );
			$objInt -> setCallable( 'Irc', 'setNick' );
			$objInt -> setMaxCalls( 1 );
			$this -> addInterval( 'keepnick', $objInt );
		}
	}


	public function irc_reply_431( V3_Event $e )
	{
		V3::log( 'Empty nickname string', V3::WARNING );
	}

	public function irc_reply_451( V3_Event $e )
	{
		// Something is wrong
		V3::log( 'You shouldn\'t send commands before you register.', V3::WARNING );
	}

	public function irc_reply_461( V3_Event $e )
	{
		// Syntax:  :Krypt.CA.US.GameSurge.net 461 * USER :Not enough parameters
	}

	public function irc_reply_471( V3_Event $e )
	{
		// Cant join chan -> +l
		// Syntax:  :DevNull.OGameNet.net 471 V3 #v3 :Cannot join channel, channel is full (+l)
		$arrChan = explode( ' ', $e -> target );
		$this -> castEvent( 'irc_cant_join_limit', array( 'chan' => $this -> getChan( $arrChan[1] ) ) );

		
	}

	public function irc_reply_472( V3_Event $e )
	{
		// unknown mode char
	}

	public function irc_reply_473( V3_Event $e )
	{
		// Cant join chan -> +i
		// Syntax: :DevNull.OGameNet.net 473 V3 #v3 :Cannot join channel, you must be invited (+i)
		$arrChan = explode( ' ', $e -> target );
		$this -> castEvent( 'irc_cant_join_invite', array( 'chan' => $this -> getChan( $arrChan[1] ) ) );
	}
	
	public function irc_reply_474( V3_Event $e )
	{
		// Cant join chan -> banned
		// Syntax: :DevNull.OGameNet.net 474 V3 #ogame.pl :Cannot join channel, you are banned (+b)
		$arrChan = explode( ' ', $e -> target );
		$this -> castEvent( 'irc_cant_join_banned', array( 'chan' => $this -> getChan( $arrChan[1] ) ) );
	}

	public function irc_reply_475( V3_Event $e )
	{
		// Cant join chan -> +k
		// Syntax: :DevNull.OGameNet.net 475 V3 #v3 :Cannot join channel, you need the correct key (+k)
		$arrChan = explode( ' ', $e -> target );
		$this -> castEvent( 'irc_cant_join_key', array( 'chan' => $this -> getChan( $arrChan[1] ) ) );
	}

	public function irc_reply_482( V3_Event $e )
	{
		// not chanOp
		// Syntax: :my.server.name 482 V3 #lodowka :You're not channel operator
	}

	public function irc_reply_501( V3_Event $e )
	{
		// Unknown user mode flag
	}

	public function irc_reply_502( V3_Event $e )
	{
		//  :krakow.irc.pl 502 V34 :Can't change mode for other users

	}
}
?>

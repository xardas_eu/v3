<?php
/**
 * 
 * Some playground
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_SandboxModule extends V3_Module
{
	private $arrConfig = array();

	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.0.1';
		$this -> deps    = array('Arc4');

		$this -> description = 'Playground for new stuff';

		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			's_test'  	   => array( 'hTest' ),
			'blockevent'   => array( 'beTest' )
		);
		return true;
	}


	public function castBlockEvent()
	{
		$this -> castEvent('blockevent',array('msg'=>'This is example of the blocking event concept'));
	}
	
	public function beTest( V3_Event $e )
	{
		V3::log( 'Received message: '. $e -> msg );
	}
	
	public function hTest( V3_Event $e )
	{
		V3::log('Notification received. Bomb is: '.$e->bomb);
		
		$strText = $this->getModule('CLI')->waitForInput( 'Enter your name' );
		
		V3::log( 'Your name is '. $strText );
		$a = 1;
		$b = 2;
		$operator = '==';
		
		
		V3::test( $a, $operator, $b );
		V3::test( 5, '==', 7 );
		V3::test( 'string', '==', 'string' );
		//V3::test( 7, '>', 6 );
		//return false;
		
		V3::log( 'Now testing blocking events.' );
	
		$this -> setWaitingFor( 'blockevent' );
		V3::log( 'Waiting handler ready, setting interval to fire the event in the next 3 seconds' );
		
		/*
		Old method:
		
		$int = new V3_Interval();
		$int -> setCallable( 'Sandbox', 'castBlockEvent' );
		$int -> setInterval( 3 );
		$int -> setMaxCalls( 1 );
		$this -> addInterval( 'tester', $int );
		*/
		
		/* Equivalent of the above code
		 * New method:
		 */
		
		$this -> runOnce( 'Sandbox', 'castBlockEvent', 3 );
		
		/* Equivalent of the normal interval (will be called 5 times) */
		//$this -> runEvery( 'Sandbox', 'castBlockEvent', 3, 5);
			
		$objE = $this -> waitForEvent();
		if( $objE instanceof V3_Event )
		{
			V3::log( 'Event came to me! Message:'. $objE -> msg );
		}
		else
		{
			V3::log( 'No event came but waitForEvent() has finished - something is wrong!', V3::WARNING );
		}
		
		V3::log( 'Testing shorthand form for intervals: V3_Accessors->runEvery()' );
		
		// will not stop control flow, runs in background
		$this -> runEvery( 'Sandbox', 'test', 1, 5, array( 'This is the test', 'of V3 timers', 'and it works!' ) );
		
		
		$this -> arc4SetKey( 'test' );
		$strText = 'some-text-input';
		$strEnc = $this -> arc4Encrypt( $strText );
		$strDec = $this -> arc4Encrypt( $strEnc );

		// test arc4
		V3::test( $strText, '==', $strDec, 'checking whether arc4 decoding gives correct results' );
		//V3::log( 'Time to test bind socket' );

		//$this -> sockTest();
	}

	public function sockTest()
	{
		// $this -> notify( 'Sandbox', 'socket_read' );

		$objS = $this -> getModule( 'Socket' );

		$objSock = $this -> socketConnect( 'irc.ogamenet.net', 6667, V3::SOCKET_TEXT_READ );
		$objSock -> write( 'USER a b c :d' );
		$objSock -> write( 'NICK zuimeji' );

		$objS -> bind( '0.0.0.0', 31817, $this -> getName(), V3::SOCKET_TEXT_READ );
/*
		$objBind = V3_Socket::factory( V3_SOCKET::SOCK_RECEIVE, array(
			'ip'     => '0.0.0.0',
			'mode'					=> AF_INET,
			'port'			    => '9084',
			'max_conns'     => 3,
			'maxconn_info'  => 'Too many connections, go away.'.V3::CRLF
		));

		$objBind -> create();
		$objBind -> bind();

		$this -> registerSocket( $objBind );
		*/
	}

	public function socket_read( V3_Event $e )
	{
		$objSock = $e -> socket;

		$strRead = trim( $objSock -> read() );
		V3::log( 'Sth came from my beloved socket: "'. $strRead.'"!' );

		/* simple ping() */

		$a = explode( ':', $strRead );
		if( $a[0] == 'PING ' )
		{
			$objSock -> write( 'PONG :'. $a[1] );
			$objSock -> write( 'JOIN #v3' );
		}

		/*
		 * simple cmd parser
		 * Format: :[server/hostmask] [command] [arg1] .. [argN] :[param]
		 * server/hostmask and param is not required
		 */

		$arrOut = V3::parseRaw( $strRead );
		print_r( $arrOut );
	}

	public function event_activate()
	{
		V3::log( 'Sandbox activated.' );


		/* This interval is going to be executed 5 times with 5 second pauses after
		 * each execution. After the last one, the V3_SandboxModule::over() method
		 * gets invoked (destructor).
		 *
		 * setArgs() populates arguments to the callable in the order specified in
		 * array (ignoring key values).
		 */

		V3::log( 'Creating interval.' );
	
		$int = new V3_Interval();
		$int -> setCallable( 'Sandbox', 'test' );
		$int -> onDelete( 'Sandbox', 'over' );
		$int -> setInterval( 1 );
		$int -> setMaxCalls( 3 );
		$int -> setArgs( array( 'a', 'b', time() ) );
		$this -> addInterval( 'test', $int );
	}

	public function test( $a, $b, $c, V3_Interval $i )
	{
		V3::log( sprintf( 'a: %s, b: %s, c: %s, calls: %s, max_calls: %s', $a, $b, $c, $i -> calls, $i -> max_calls ) );
	}

	public function over( $a, $b, $c, V3_Interval $i )
	{
		V3::log( 'Interval is over.' );

		v3::log( 'notify test' );
		$this -> notify( 'Sandbox', 's_test', array('bomb'=>time()));
	}
}

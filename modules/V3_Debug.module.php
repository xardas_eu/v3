<?php
/**
 * 
 * V3 debugger module. Provides simple unit-tests, message logging and displaying messages.
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_DebugModule extends V3_Module
{
	
	const TIMESTAMP_FORMAT = 'H:i:s';

    public function _init()
    {
        $this -> author = 'xardas';
        $this -> version = '0.8';
        $this -> deps = array( 'CLI' );

		$this -> description = 'Debugging & Logging module';

		$this -> default_config = array(	
			'log_levels' 		=> '1, 2, 4, 8, 16',
			'file_log_levels'	=> '1, 16, 32, 64',
			'file'				=> '%v3_root%/logs/%botname%-%date%.log',
			'display_php_errors'=> false
		);
		
        $this -> compat  = array( '0.1.0', '0.2.0' );
        $this -> events = array(
            'log' => array( 'event_log' ),
            'test' => array( 'event_test' ),
            'module_registered' => array( 'event_module_registered' ),
            'deactivate' => array( 'event_deactivate' )
        );

        $this -> log_levels = array( V3::INFO, V3::ERROR, V3::FATAL );
        $this -> file_log_levels = array( );
        return true;
    }

    public function event_module_registered(V3_Event $e )
    {
        if ( $e -> name == 'Config' )
        {
            $this -> log_levels = explode( ',', str_replace( ' ', '', $this -> getConf( 'log_levels' ) ) );
            $this -> file_log_levels = explode( ',', str_replace( ' ', '', $this -> getConf( 'file_log_levels' ) ) );
            $this -> logfile = $this -> getConf( 'file' );

            if ( $this -> logfile != '' )
            {
                V3::log( 'Opening logfile: ' . $this -> logfile );
                $this -> file_handle = fopen( $this -> logfile, 'a+' );
            }
        }
    }

    public function event_activate()
    {
        pakecolor::style( 'default', array( 'fg' => 'white' ) );
        pakecolor::style( V3::INFO, array( 'fg' => 'green' ) );
        pakecolor::style( V3::DEBUG, array( 'fg' => 'blue' ) );
        pakecolor::style( V3::WARNING, array( 'fg' => 'white', 'bg' => 'red' ) );
        pakecolor::style( V3::NOTICE, array( 'fg' => 'yellow' ) );
        pakecolor::style( V3::ERROR, array( 'fg' => 'red', 'bold' => true ) );
        pakecolor::style( V3::FATAL, array( 'fg' => 'red', 'bg' => 'white', 'bold' => true ) );

        // tests
        pakecolor::style( '_pass', array( 'fg' => 'green' ) );
        pakecolor::style( '_fail', array( 'bold' => 'true', 'fg' => 'red' ) );
    }

    public function event_deactivate()
    {
        if ( !$this -> hasProperty( 'file_handle' ) )
        {
            return;
        }
        V3::log( 'Closing logfile: ' . $this -> logfile );
        fclose( $this -> file_handle );
        $this -> delProperty( 'file_handle' );
    }

    public function event_test(V3_Event $e )
    {
        $strString = sprintf( 'Test "%s" %s "%s" (%s): ', $e -> arg1, $e -> operator, $e -> arg2, $e -> description );
        $blnPass = false;

        if ( is_string( $e -> arg1 ) )
        {
            $e -> arg1 = '\'' . $e -> arg1 . '\'';
        }

        if ( is_string( $e -> arg2 ) )
        {
            $e -> arg2 = '\'' . $e -> arg2 . '\'';
        }

        if ( !empty( $e -> operator ) )
        {
            eval( sprintf( '$blnPass = ((%s %s %s));', $e -> arg1, $e -> operator, $e -> arg2 ) );
            //die('did some evalin');
        }
        else
        {
            $blnPass = (bool) $e -> arg1;
//			die('did some scopin');
        }

        $strString .= $blnPass ? pakeColor::colorize( 'PASSED', '_pass' ) : pakeColor::colorize( 'FAILED', '_fail' );

//		echo 'GONNA DO SOME LOGGIN';
        V3::log( $strString, $blnPass ? V3::VERBOSE : V3::WARNING );
    }

    public function event_log(V3_Event $e )
    {
        //level

        if ( $e -> level == V3::VERBOSE AND $this -> getModule( 'CLI' ) -> hasArgument( 'noverbose' ) )
        {
            return false;
        }
        if ( $e -> level == V3::DEBUG AND $this -> getModule( 'CLI' ) -> hasArgument( 'nodebug' ) )
        {
            return false;
        }

        if ( $this -> getModule('CLI') -> hasArgument( 'q' ) OR $this -> getModule('CLI') -> hasArgument( 'quiet' ) )
        {
            return false;
        }



        $strMsg = '';
        switch ( $e -> level )
        {
            case V3::INFO:
            default:
                $strMsg .= pakeColor::colorize( '[+]', V3::INFO );
                break;
            case V3::DEBUG:
                $strMsg .= pakeColor::colorize( '[?]', V3::DEBUG );
                $e -> msg = V3::debug_caller_name2() . $e -> msg;
                break;
            case V3::NOTICE:
                $strMsg .= pakeColor::colorize( '[*]', V3::NOTICE );
                break;
            case V3::WARNING:
                $strMsg .= pakeColor::colorize( '[!]', V3::WARNING );
                break;
            case V3::ERROR;
                $strMsg .= pakeColor::colorize( '[!!!]', V3::ERROR );
                break;
            case V3::FATAL:
                $strMsg .= pakeColor::colorize( '[R.I.P]', V3::FATAL );
        }

        
        $timestamp = '['. date( self::TIMESTAMP_FORMAT ).']';

        if ( $this -> getModule( 'CLI' ) -> hasArgument( 'trace' ) )
        {
            $e -> msg = V3::debug_caller_name2() . $e -> msg;
        }

        $strMsg .= ' ' . pakeColor::colorize( $e -> msg, 'default' );
        $strMsg = $timestamp.$strMsg;
        
        if ( in_array( $e -> level, $this -> log_levels ) )
        {
            echo $strMsg . V3::CRLF;
        }

        if ( in_array( $e -> level, $this -> file_log_levels ) && $this -> hasProperty( 'file_handle' ) )
        {

            fputs( $this -> file_handle, '[' . date( V3::DEBUG_TIMESTAMP_FORMAT ) . '] ' . $e -> msg . V3::CRLF );
        }
    }

}

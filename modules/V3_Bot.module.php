<?php
/**
 * IRCBot module. It was the module to run which V3 was actually designed for.
 *
 * @author xardas
 * @package V3
 * @subpackage Modules
 * @see V3_IRCModule
 */
class V3_BotModule extends V3_Module
{
	private $arrConfig = array();
	private $arrJoinQueue = array();


	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.1.0';
		$this -> deps    = array( 'Irc', 'Userlist', 'Chanlist' );

		$this -> description = 'Dummy bot implementation based on IRC module';

		
		$this -> default_config = array(
			'rejoin_delay'	=> 5,
			'away_string'	=> 'V3 away!'
		);
		
		
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			'joined_irc' => array( 'joinedIrc' ),
			'irc_chanmsg'	=> true,
			'irc_joined_chan_self' => true,
			'irc_parted_chan_self' => true,
			'irc_kick_self'	=> true,
			'irc_cant_join_key' => array('cannotJoin'),
			'irc_cant_join_invite' => array('cannotJoin'),
			'irc_cant_join_banned' => array('cannotJoin'),
			'irc_cant_join_limit'	=> array('cannotJoin')
		);
		return true;
	}
	
	public function cannotJoin( V3_Event $e )
	{
		$this -> tryToRejoin( $e -> chan );
	}

	public function joinQueue( V3_Interval $i )
	{
		$strChan = array_shift( $this -> arrJoinQueue );
		if( $strChan )
		{
			$this -> getIrc() -> joinChan( $strChan );
		}
		else
		{
			$i -> suspend();
		}
	}

	public function tryToRejoin( V3_Channel $objChan )
	{
		if( ! array_search( $objChan -> name, $this -> arrJoinQueue ) )
		{
			array_push( $this -> arrJoinQueue, $objChan );
			$this -> join_interval -> unsuspend();
			
			return true;
		}
		return false;
	}

	public function handle_irc_kick_self( V3_Event $e )
	{
		V3::log( 'Got kicked from ' . $e -> chan -> name . ', rejoining !', V3::INFO );
		$this -> tryToRejoin( $e -> chan );
	}


	public function handle_irc_joined_chan_self( V3_Event $e )
	{
		if( $this -> getChanlist() )
		{
			$this -> getChanlist() -> addChan( $e -> chan -> name, $e -> chan );
		}
	}

	public function handle_irc_parted_chan_self( V3_Event $e )
	{
		if( $this -> getChanlist() )
		{
			$this -> getChanlist() -> delChan( $e -> chan -> name);
		}
	}

	public function event_activate()
	{
		// V3::log( $this -> getModuleConf( 'nick' ) );
		$objIrc = $this -> getIrc();
		$objIrc -> connect();
		
		$intDelay = (int)$this -> getConf( 'rejoin_delay', 1);
		$this -> join_interval = $this -> runEvery($this,'joinQueue', $intDelay,0);
	
	}

	public function joinedIrc( V3_Event $e )
	{

		$objSock = $e -> socket;
		V3::log( sprintf( 'Bot has joined irc on %s:%s as %s.', $this -> getIrc() -> getCurrentServer(), $this -> getIrc() -> getPort(), $this -> getIrc() -> getNick() ), V3::INFO );

		$strAuth = $this -> getModuleConf( 'authserv_authname' );
		if( $strAuth )
		{
			$strService = $this -> getModuleConf( 'authserv_service' );
			$this -> getIrc() -> privmsg( $strService,
			sprintf( '%s %s %s',
			$this -> getModuleConf('authserv_command'),
			$strAuth,
			$this -> getModuleConf( 'authserv_password' )
			) );
			V3::log( 'Authenticating to '. $strService.' as '. $strAuth, V3::INFO );
		}

		$strAway = $this -> getModuleConf( 'away_string' );
		if( $strAway )
		{
			$this -> getIrc() -> setAway( $strAway );
		}


		foreach( $this -> getChanlist() -> getChannels() as $objChannel )
		{
			/** That is VERY simple and yet channelkey-aware join. */
			$this -> getIrc() -> scheduleJoin( $objChannel  );
				
			/** temp **/
			if( $e -> isFirstRun() )
			{
				$this -> runEvery( $this, 'shortStats', 30, 0, array( $objChannel -> name ) );
			}
		}

		/**
		 * Start first channel join and so on
		 */
		$this -> getIrc() -> scheduledJoins();

	}

	public function handle_irc_chanmsg( V3_Event $e )
	{
		if( $this -> getIrc() -> joinedChan( $e -> chan -> name) )
		{
			if( $e->msg == 'part')
			{
				$this -> getIrc() -> partChan($e->chan->name);
			}
				
			if( $e -> msg == 'users' )
			{
				$this -> getIrc() -> privmsg( $e -> chan -> name, print_r( $this -> getIrc() -> getUsers(), true ) );

			}
				
			$str = substr( $e -> msg , 0, 6);
			if( $str == 'filter' )
			{
				$msg  = explode( ' ', $e->msg);

				$this -> getIrc() -> privmsg( $e -> chan -> name,
					'FILTER: '.$msg[1].PHP_EOL.print_r( $e -> chan -> getUsers() -> filter( $msg[1]), true )
				);
			}
				
			if( $str == 'whois ' )
			{
				$msg  = explode( ' ', $e->msg);

				$this -> getIrc() -> privmsg( $e -> chan -> name,
					'FILTER: '.$msg[1].PHP_EOL.print_r( $this->getIrc()->whois($msg[1]), true )
				);
			}

			if( $e -> msg == 'chans' )
			{
				$this -> getIrc() -> privmsg( $e -> chan -> name, print_r( $this -> getIrc() -> getChans(), true ) );

			}
				
			$this -> getIrc() -> privmsg( $e -> chan -> name, $e->user->nick.', why did you say: '. $e -> msg.' ?' );
		}
		//print_r($e);
	}


	public function shortStats( $strChan, V3_Interval $i )
	{
		$this -> getIrc() -> privmsg( $strChan, sprintf(
			'Bot name: %s | Bot nick: %s | Bot version: %s | Memory usage: %d KB | Uptime: %s | Channel: %s | Repeat every: %d seconds',
		$this -> getModule('CLI') -> getArgument( 'name' ),
		$this -> getIrc() -> getNick(),
		V3::BOT_VERSION,
		ceil(memory_get_usage(true)/1024),
		$this -> getCore() -> getUptime( true ),
		$strChan,
		$i -> getInterval()
		));
	}
}
?>

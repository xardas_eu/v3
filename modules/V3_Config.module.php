<?php
/**
 *
 * V3 configuration backbone. Stores parameters for all modules, also the module list itself
 *
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
class V3_ConfigModule extends V3_Module
{
	private $arrConfig = array();
	private $blnCreateNew = false;


	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '1.0';

		$this -> description = 'Stores global and local configuration';

		$this -> file    = V3_DIR.'/config/'. $this -> getModule( 'CLI' ) -> getArgument( 'name' ).'.conf';
		$this -> deps    = array(null);
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
		);
		return true;
	}

	public function hasConfigFor( $strBotName )
	{
		return is_readable( V3_DIR.'/config/'. $strBotName.'.conf' );
	}

	public function writeConfig( $strConfig )
	{
		return file_put_contents( $this -> file , $strConfig ) && $this->loadConfig();
		
	}

	protected function _config( $arrArgs ) {

		if( isset( $arrArgs['blnCreateNew'] ) ) {
			$this -> blnCreateNew = $arrArgs['blnCreateNew'];
		}
	}

	public function getModuleConf( $strModule )
	{
		if( !isset( $this -> arrConfig[ $strModule ] ) )
		{
			V3::log( 'I don\'t have config for module '. $strModule, V3::WARNING );
			return false;
		}

		return $this -> arrConfig[ $strModule ];
	}

	public function replaceConstants( $strLine )
	{
		return str_replace(
		array( 		 '%botname%',
						 '%date%',
						 '%botversion%',
						 '%v3_root%'
						 ),
						 array( $this -> getModule( 'CLI' ) -> getArgument( 'name' ),
						 date( 'd-m-Y' ),
						 V3::BOT_VERSION,
						 V3_DIR
						 ),
						 $strLine );
	}

	public function getConf( $strModule, $strVar )
	{
		if( !isset( $this -> arrConfig[ $strModule ] ) )
		{
			V3::log( 'I don\'t have config for module '. $strModule, V3::WARNING );
			return false;
		}

		if( !isset( $this -> arrConfig[ $strModule ][ $strVar ] ) )
		{
			V3::log( sprintf( 'Unknown config directive "%s" for module "%s"', $strVar, $strModule ), V3::NOTICE );
			return false;
		}

		return $this -> arrConfig[ $strModule ][ $strVar ];
	}

	public function event_activate( V3_Event $e )
	{
		$this -> loadConfig();

	}

	public function setConfFor( $strModule, $strKey, $mixValue )
	{
		switch( gettype($mixValue) )
					{
						case 'integer':
							$strConf = $mixValue;
							break;
						case 'string':
							$strConf = '"'.$mixValue.'"';
							break;
						case 'boolean':
							$strConf = $mixValue ? 'true' : 'false';
							break;

						default:
							V3::log('Unknown configuration type: '. gettype($mixValue));
							exit;
							break;
					}

		$this -> arrConfig[$strModule][$strKey] = $strConf; // na jana
			}

	private function createNewConf()
	{
		V3::log( 'Creating new configuration file..' );
		file_put_contents( $this -> file, '' );
	}

	public function loadConfig()
	{
		V3::log( 'Loading configuration file' );
		if( !file_exists( $this -> file ) OR !is_readable( $this -> file ) )
		{
			if( $this -> blnCreateNew )
			{
				$this -> createNewConf();
			}
			else
			{
				throw new Exception( sprintf( 'Config file "%s" is not readable or doesn\'t exist.', $this -> file ) );
			}
		}

		$this -> arrConfig = parse_ini_file( $this -> file, true );
		foreach( $this -> arrConfig as $strMod => $arrMod )
		{
			foreach( $arrMod as $strK => $strV )
			{
				$this -> arrConfig[ $strMod ][ $strK ] = $this -> replaceConstants( $strV );
			}
		}


		// Allow passing config parameters from CLI arguments
		foreach( $this -> getModule('CLI') -> getArguments() as $strArg => $strVal )
		{
			if( strpos( $strArg, '.' ) !== FALSE )
			{
				$arrArg = explode( '.', $strArg, 2 );
				// make the first letter capital as it's the module name
				$this -> arrConfig[ ucfirst( $arrArg[ 0 ] ) ][ $arrArg[ 1 ] ] = $this -> replaceConstants( $strVal );
			}
		}

		//V3::log( print_r( $this -> arrConfig, true ) );
	}
}

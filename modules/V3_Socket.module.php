<?php
/**
 * 
 * V3 socket module. Actually integral part of the application - as most V3 uses will involve sockets.
 * 
 * @author xardas
 * @package V3
 * @subpackage Modules
 */
final class V3_SocketModule extends V3_Module
{
	const MAX_CONN_QUEUE = 10;

	private $arrSockets = array( 'connect' => array(), 'bind' => array() );

	public function _init()
	{
		$this -> author  = 'xardas';
		$this -> version = '0.0.1';
		$this -> deps    = array(null);
		$this -> compat  = array( '0.1.0', '0.2.0' );
		$this -> events  = array(
			'idle'				 => array( 'idle', 'idleReads'  ),
			'deactivate'		=> true
		);
		return true;
	}

	public function handle_deactivate()
	{
		V3::log( 'Closing all sockets', V3::VERBOSE );
		foreach( $this -> arrSockets['connect'] as $objSock )
		{
			$objSock -> close();
		}

		foreach( $this -> arrSockets['bind'] as $objSock )
		{
			foreach( $objSock -> clients as $objClient )
			{
				$objClient -> close(); // solves problem with "Address already in use"
			}
				
			$objSock -> close();
		}
	}

	private static function socketError( $resSocket = null )
	{
		return socket_strerror( socket_last_error( $resSocket ));
	}

	public function event_activate( V3_Event $e )
	{
		V3_Loader::import( 'modules.socket.*');
	}

	/*
	 * WARNING: that function blocks.
	 */
	public function connect( $strHost, $intPort, $strModule, $strMode, $strIp = null, $strCMode = AF_INET )
	{
		V3::log( sprintf( 'Connecting to %s:%s for %s from ip %s ',$strHost,$intPort,$strModule,$strIp), V3::VERBOSE );

		if( !( $resSocket = @socket_create( $strCMode, SOCK_STREAM, SOL_TCP ) ) )
		{
			V3::log('error@socket_create: '. self::socketError( $resSocket ), V3::WARNING );
			return false;
		}

		if( $strIp )
		{
			if( !@socket_bind( $resSocket, $strIp ) )
			{
				V3::log( 'error@socket_bind: '. self::socketError( $resSocket ), V3::WARNING );
				return false;
			}
		}

		if( !@socket_connect( $resSocket, $strHost, $intPort ) )
		{
			V3::log( 'error@socket_connect: '. self::socketError( $resSocket ), V3::WARNING );
			return false;
		}

		if( !@socket_set_option( $resSocket, SOL_SOCKET, SO_REUSEADDR, true ) ||
		!@socket_set_nonblock( $resSocket ) )
		{
			V3::log( 'error@socket_set_option: '. self::socketError( $resSocket), V3::WARNING );
		}

		$intId = sizeof( $this -> arrSockets['connect'] );

		$this -> arrSockets['connect'][ $intId ] = new V3_Socket( array(
		'socket' => $resSocket,
		'sock_id'=> $intId,
		'type' => 'connect',
		'mode' => $strMode,
		'module' => $strModule,
		'host' => $strHost,
		'port' => $intPort,
		'read_data' => ''
		) );

		//print_r($this->arrSockets);

		$this -> notify( $strModule, 'socket_connected', array( 'socket' => $this -> arrSockets['connect'][$intId] ) );

		unset( $strHost, $intPort, $strMode, $strModule, $strIp );

		return $this -> arrSockets['connect'][$intId];
	}

	public function bind( $strHost, $intPort, $strModule, $strMode, $intMaxConn = self::MAX_CONN_QUEUE, $strMaxConnInfo = 'Too many connections.', $strBMode = AF_INET )
	{
		V3::log( sprintf( 'Bound %s:%s for %s (max_conn=%s)', $strHost, $intPort, $strModule, $intMaxConn ), V3::VERBOSE );

		if( !( $resSocket = @socket_create( $strBMode, SOCK_STREAM, SOL_TCP ) ) )
		{
			V3::log('error@socket_create: '. self::socketError( $resSocket ), V3::WARNING );
			return false;
		}

		if( !@socket_bind( $resSocket, $strHost, $intPort ) )
		{
			V3::log( 'error@socket_bind: '. self::socketError( $resSocket ), V3::WARNING );
			return false;
		}

		if( !@socket_listen( $resSocket, $intMaxConn ) )
		{
			V3::log( 'error@socket_listen:' . self::socketError($resSocket), V3::WARNING );
			return false;
		}

		$arrOpt = array('l_onoff' => 1, 'l_linger' => 1);
		@socket_set_option( $resSocket, SOL_SOCKET, SO_LINGER, $arrOpt );

		if( !@socket_set_option( $resSocket, SOL_SOCKET, SO_REUSEADDR, 1 ) ||
		!@socket_set_nonblock( $resSocket ) )
		{
			V3::log( 'error@socket_set_option: '. self::socketError( $resSocket), V3::WARNING );
			return false;
		}

		$intId = sizeof( $this -> arrSockets['bind'] );
		$this -> arrSockets['bind'][$intId] = new V3_Socket( array(
			'socket' => $resSocket,
			'sock_id'=> $intId,
			'type' => 'bind',
			'clients'	=> array(),
			'mode' => $strMode,
			'module' => $strModule,
			'host' => $strHost,
			'port' => $intPort,
			'conns' => 0,
			'limit' => $intMaxConn,
			'toomany' => $strMaxConnInfo
		) );


		$this -> notify( $strModule, 'socket_bound', array('socket' => $this -> arrSockets['bind'][$intId] ));
		unset( $strMaxConnInfo, $intMaxConn, $intPort, $strHost, $strModule, $strMode );
		return $this -> arrSockets['bind'][$intId];
	}

	public function getSocks()
	{
		$arrSockets = array();
		foreach( (array)$this -> arrSockets['connect'] as $intId => $objSocket )
		{
			if( is_resource( $objSocket -> socket ) )
			{
				$arrSockets[] = $objSocket -> socket;
			}
			else
			{
				unset( $this -> arrSockets['connect'][$intId] );
			}
		}

		foreach( (array)$this -> arrSockets['bind'] as $intId => $objSocket )
		{
			if( is_resource( $objSocket -> socket ) )
			{
				$arrSockets[] = $objSocket -> socket;

				foreach( $objSocket -> clients as $objClient )
				{
					$arrSockets[] = $objClient -> socket;
				}
			}
			else
			{
				unset( $this -> arrSockets['bind'][$intId]);
			}
		}

		return $arrSockets;
	}

	public function getSocket( $resSocket )
	{
		foreach( $this -> arrSockets['connect'] as $intId => &$objSocket )
		{
			if(  $objSocket -> socket == $resSocket )
			{
				return $objSocket;
			}
		}

		foreach( $this -> arrSockets['bind'] as $intId => &$objSocket )
		{
			if( (string)$objSocket -> socket == (string)$resSocket )
			{
				return $objSocket;
			}
				
			foreach( $objSocket -> clients as $objClient )
			{
				if( (string)$objClient -> socket == (string)$resSocket )
				{
					return $objClient;
				}
			}
		}

		return false;
	}

	public function idle()
	{
		$arrSocks = $this -> getSocks();
		if( empty( $arrSocks ) )
		{
			return false;
		}
		if( socket_select( $arrSocks, $arrWrite, $arrT, 0 ) > 0 )
		{
			$blnHandled = false;
			foreach( $arrSocks as $resSocket )
			{
				$objSocket = $this -> getSocket( $resSocket );
				if( !$objSocket instanceof V3_Socket )
				{
					throw new Exception(
					sprintf( 'Something impossible has happened: socket "%s" is not a socket!', (string)$resSocket )
					);
					continue;
				}

				if( $objSocket -> type == 'connect' )
				{
					$this -> doConnect( $objSocket );
					$blnHandled = true;
				}
				elseif( $objSocket -> type == 'bind' )
				{
						
						
						
					$resClientSocket = socket_accept( $resSocket );
					socket_getpeername( $resClientSocket , $strHost, $intPort );
					$objClientSocket = new V3_Socket( array(
						'socket' => $resClientSocket,
						'type'   => 'client',
						'mode'   => $objSocket -> mode,
						'module' => $objSocket -> module,
						'host'	 => $strHost,
						'port'	 => $intPort,
						'mother' => $objSocket,
						'first_read'=>true,
						'read_data' => '',
						'sock_id'=> $strHost.':'.$intPort
					) );
						
					if( count( $objSocket -> clients ) >= $objSocket -> limit )
					{
						/**
						 * We can't allow anymore connections to that socket
						 */
						
						V3::log( sprintf( 'Socket "%s" for module %s had to be rejected for exceeding limit(%s)',
							$objClientSocket -> sock_id,
							$objClientSocket -> module,
							$objSocket -> limit
							
						 ), V3::DEBUG );
						 
						$objClientSocket -> write( $objSocket -> toomany );
						$objClientSocket -> close();
						
						return;
					}
						
					V3::log( sprintf(
						'Accepted incoming connection from %s:%s for module %s',
					$strHost, $intPort, $objSocket -> module
					), V3::DEBUG );
						
					$this -> notify( $objSocket -> module, 'socket_accepted', array('socket'=>$objClientSocket) );
						
					$objSocket -> __set( 'clients', array_merge( $objSocket->clients, array( $strHost.':'.$intPort => $objClientSocket ) ) );
						
						
					$blnHandled = true;
				}
				elseif( $objSocket -> type == 'client' )
				{
					$this -> handleClient( $objSocket );
				}

				else
				{
					V3::log( 'Unknown socket type: '. var_dump( $objSocket ).' ! '.(string)$resSocket );
				}
			}
				
			if( !$blnHandled )
			{
				/**
				 * 100% sure it is the child of some bind socket
				 * so let's do some painful and SLOOOOW analysis
				 * */

				//$objSocket = $this -> retrieveChildSocket();
			}
		}
	}

	private function handleClient( V3_Socket &$objSock )
	{
		$strData = socket_read( $objSock -> socket, 65536, $objSock -> mode );
		/** 
		 * Does not work on V3::SOCKET_BINARY_READ
		 */
		
		if( $strData === false && !$objSock -> first_read)
		{
			V3::log( sprintf( 'Closing child socket %s:%s for module %s (client disconnected)',
			$objSock->host,$objSock->port,$objSock->module ), V3::DEBUG );
			$objSock -> close();
			$mother = $this -> getSocket( $objSock -> mother -> socket );
			$children = $mother->clients;
			unset($children[$objSock->host.':'.$objSock->port]);
			$mother -> __set( 'clients', $children );
		}

		$strData = trim( $strData );
		$objSock -> __set( 'first_read', false );
		$objSock -> __set( 'read_data', $objSock -> __get( 'read_data'). $strData );
		return true;
	}

	public function doConnect( V3_Socket &$objSock )
	{
		$strData = socket_read( $objSock -> socket, 65536, $objSock -> mode );

		if( empty( $strData ) )
		{
			// Socket should be closed.
			$objSock -> close();

			// And removed
			unset( $this -> arrSockets[ $objSock -> type ][ $objSock -> sock_id ] );
			return false;
		}

		$strData = trim( $strData );

		$objSock -> __set( 'read_data', $objSock -> __get( 'read_data'). $strData );
		return true;
	}

	public function idleReads( V3_Event $e )
	{
		foreach( $this -> getSocks() as $resSock )
		{
			$objSock = $this -> getSocket( $resSock );
			if( !empty( $objSock -> read_data ) )
			{
				$this -> notify( $objSock -> module, 'socket_read', array( 'socket' => $objSock ) );
			}
		}

	}

}
?>

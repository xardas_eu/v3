<?php
 $tests = array(
   ":OGN2.OGameNet.net 372 zuimeji :TRUE",
   "NOTICE AUTH :*** Looking up your hostname",
   "PING :sometext",
//   ":Nick!Host@name.here PRIVMSG #channel :Good morning world",
   ":Nick!Host@name.here JOIN :#channel",
   ":Nick!Host@name.here PART #chan",
   ":Nick!Host@name.here PART #chan :reason",
//   ":NickServ!Host@name.here NOTICE Nick :Password accepted - you are now recognized.",
//   ":Nick!Host@name.here MODE #channel +ac-bd",
//   ":Nick!Host@name.here TOPIC #chan :Message for new Topic",
//   ":Nick!Host@name.here KICK #chan a,b,c,d :test",
   ":Nick!Host@name.here SHIT a b c d e :test",
   ":OGN3.PL.Net 474 xardas :you rock",
 );


function parseRaw($msg)
{
	if( preg_match( '/^(\:.*?\s){0,1}([A-Z]+|[0-9]{1,3})\s{0,1}(.*?)(\s\:.*?){0,1}$/', $msg, $out ) )
	{
		if( isset($out[4]))
		{
			$out[4] = substr( trim($out[4]), 1 );
		}

		if( strpos( $out[3], ':' ) === (int)0 )
		{
			$out[3] = substr( $out[3], 1 );
		}
		unset( $out[0] );
		ksort( $out );
		return $out;
	}
	else
	{
		// not proper raw
		return false;
	}
}

$regex = '/^(\:.*?\s){0,1}([A-Z]+|[0-9]{1,3})\s{0,1}(.*?)(\s\:.*?){0,1}$/';
print "Regex: $regex\n";  
foreach($tests as $t) {
  print "Matching: $t\n";
  print_r( parseRaw($t));
 }
 ?>

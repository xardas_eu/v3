<?php

// tick use required as of PHP 4.3.0
declare(ticks = 1);

// signal handler function
function sig_handler($signo)
{

         switch ($signo) {
                 case SIGINT:
                         // handle shutdown tasks
                         echo "Keyboard pressed...\n";
                         exit;
                         break;
                 default:
                         // handle all other signals
         }

}

echo "Installing signal handler...\n";

// setup signal handlers
pcntl_signal(SIGINT, "sig_handler");

// or use an object, available as of PHP 4.3.0
// pcntl_signal(SIGUSR1, array($obj, "do_something");

// Just a stupid simple loop
for ($i = 1; $i <= 10; $i++){
    echo ".";
    sleep(1);
   
}

echo "Done\n"

?>

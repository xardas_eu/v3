<?php
class V3_RPCCommandChain extends V3_Accessors
{
	private $arrStack = array();
	private $strName = null;

	public function __construct( $strName )
	{
		$this -> strName = $strName;
	}

	public function getName()
	{
		return $this -> strName;
	}

	public function add( $strMethod, array $arrArgs = array() )
	{
		$this -> arrStack[ sizeof( $this -> arrStack ) ] = array( 'method' => $strMethod, 'args' => $arrArgs );
		return $this;
	}

	public function execute()
	{
		V3::log( 'Executig RPC command chain: ' . $this -> getName(), V3::VERBOSE );
		$intSize = sizeof( $this -> arrStack );
		$objObject = $this;
		foreach( $this -> arrStack as $intId => $arrCall )
		{
			if( $intId == ( $intSize - 1 ) )
			{
				return call_user_func_array( array( $objObject, $arrCall[ 'method' ] ), $arrCall[ 'args' ] );
			}
			$objObject = call_user_func_array( array( $objObject, $arrCall[ 'method' ] ), $arrCall[ 'args' ] );
		}
	}
}
<?php
/**
 *
 * Not what it looks like - for throwing purposes I still use plain old PHP Exception.
 * This is just some handlers for error_reporting and exception logging.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
class V3_Exception extends V3_Accessors
{
	public static function bindHandlers()
	{
		set_error_handler( array( new V3_Exception, 'handleError'));
		set_exception_handler( array( new V3_Exception, 'handleException'));
	}
	
	public function handleError( $intNo, $strError, $strFile, $intLine )
	{
		if( class_exists( 'V3' ) )
		{
			V3::castEvent( 'error', array( 
				'no'          => $intNo,
				'error'       => $strError,
				'file'				=> $strFile,
				'line'				=> $intLine
				) );
			
			if( V3::getCore() && V3::getCore()->getConfig() instanceof V3_ConfigModule && V3::getCore()->getConfig()->getConf('Debug','display_php_errors') )
			{
				V3::log( sprintf( 'Error no. %s in file "%s" on line %s: %s', $intNo, $strFile, $intLine, $strError ), V3::DEBUG );
			}
		}
		else
		{
			echo sprintf( 'Error no. %s in file "%s" on line %s: %s', $intNo, $strFile, $intLine, $strError)."\r\n";
		}
	}
	
	public function handleException( Exception $objE )
	{
		$arrTrace = $objE->getTrace();
		if( class_exists( 'V3' ) )
		{
			$strTrace = V3::generateTraceString( $arrTrace[0] );
			V3::castEvent( 'exception', array( 'exception' => $objE ) );
			
			V3::log( 'Exception: '. $strTrace.$objE -> getMessage(), V3::FATAL );
			if( $this -> getCore() )
			{
				//$this -> getCore() -> cleanup();
				$this -> getCore() -> terminate( 'Exception thrown.' );
			}
		}
		else
		{
			$strTrace = '*';
			echo 'V3::Exception: '. $objE->getMessage()."\r\n";
		//	exit;
		}
		
		
		return true;
	}
}

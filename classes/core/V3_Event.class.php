<?php
/**
 *
 * The very foundation of V3 concept - event-driven platform. Passes any information to virtually any module, 
 * in real-time, non-blocking manner.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
class V3_Event extends V3_Accessors
{
	private $strName = '';
	private $blnPopulate = true;
	private $objCaller;

	public function getCaller()
	{
		return $this -> objCaller;
	}

	public function __construct( $strName, $arrArgs = array(), $blnPopulate = true, $intTimes = 1, $objCaller = null )
	{
		$this -> strName = $strName;
		$this -> objCaller = $objCaller;
		foreach( $arrArgs as $strArg => $strVal )
		{
			$this -> __set( $strArg, $strVal );
		}
		$this -> __set( 'times', $intTimes );
		$this -> blnPopulate = $blnPopulate;
	}

	public function getTimesCalled()
	{
		return $this -> times;
	}

	public function isFirstRun()
	{
		return ( $this -> getTimesCalled() == 1 );
	}

	public function getName()
	{
		return $this -> strName;
	}

	public function getPopulate()
	{
		return $this -> blnPopulate;
	}

	public function setPopulate( $blnPop = true )
	{
		$this -> blnPopulate = $blnPop;
	}
}

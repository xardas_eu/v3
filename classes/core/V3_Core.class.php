<?php
/**
 *
 * Core.
 * Holds instances of every registered module, shared variables, uptime, intervals and many, many more.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
class V3_Core implements ArrayAccess
{
	/*
	 * Example module:
	 * $this -> arrModules[ 'example' ] = array(
	 * 		'object'   => V3_Module object,
	 * name'     => 'example',
	 * version'  => '0.0.2',
	 * compat'   => array( '0.0.1', '0.0.5' ),
	 * author'   => 'xardas',
	 * events'   => array( 'idle', 'log', 'etc' ),
	 * deps'     => array( 'foo', 'bar' ),
	 * can_unreg'=> true,
	 * )
	 */

	private $arrModules = array( );
	private $blnTerminated = false;
	private $arrIntervals = array( );
	private $intStartTime = 0;

	private static $objSharedMem = null;

	private $arrSharedObjects = array();
	private $eventList = array();

	public function getEventList ()
	{
		return $this->eventList;
	}


	public function offsetExists( $strKey )
	{
		return isset( $this -> arrSharedObjects[ $strKey] );
	}

	public function offsetSet( $strKey, $mixData )
	{
		return $this -> persist( $strKey, $mixData );
	}

	public function offsetGet( $strKey )
	{
		return $this -> retrieve( $strKey );
	}

	public function offsetUnset( $strKey )
	{
		return $this -> unlink( $strKey );
	}


	public function persist( $strName, $mixData )
	{
		$this -> arrSharedObjects[$strName] = $mixData;
		return true;
	}

	public function unlink( $strName )
	{
		if( isset( $this -> arrSharedObjects[ $strName ] ) )
		{
			unset( $this->arrSharedObjects[$strName] );
			return true;
		}
			
		V3::log( sprintf( 'V3_Core has no shared object named "%s"', $strName ), V3::WARNING );
		return false;
	}

	public function retrieve( $strName )
	{
		if( isset( $this -> arrSharedObjects[ $strName ] ) )
		{
			return $this->arrSharedObjects[$strName];
		}
			
		throw new Exception( sprintf( 'V3_Core has no shared object named "%s"', $strName ) );
		return false;
	}

	/**
	 *
	 * Działa tylko z --with-shmop
	 */
	public static function getSharedMemory()
	{
		if( !function_exists( 'shmop_open' ) )
		{
			throw new Exception( 'Your OS/PHP does not support shared memory for PHP (shmop)' );
		}
			
		if( empty(self::$objSharedMem) )
		{
			self::$objSharedMem = new V3_SharedMemory();
			self::$objSharedMem -> initialize( 'V3sharedMem' );
		}
			
		return self::$objSharedMem;
	}

	/*
	 * foreach( $this -> arrIntervals as $strHandle => $arrData )
	 {
	 * if( time() - $arrData['interval'] >= $arrData['last'] )
	 {
	 * if( !empty( $arrData['func']))
	 {
	 * call_user_func_array($arrData['func'],$arrData['args']);
	 }
	 * $this -> arrIntervals[ $strHandle ]['first'] = 0;
	 * $this -> arrIntervals[ $strHandle ]['last'] = time();
	 }
	 }
	 */

	public function daemonCheck( V3_CLIModule $objCLI )
	{
		$blnTerm = false;
		if ( $objCLI -> hasArgument( 'd' ) || $objCLI -> hasArgument( 'daemon' ) )
		{
			if ( $objCLI -> askYesNo( '*]' . PHP_EOL . 'V3 Daemon support is ' . pakeColor::colorize( 'EXPERIMENTAL', array( 'fg' => 'red' ) ) . ' Are you sure you want to try?' . PHP_EOL .
                            'There will be ' . pakeColor::colorize( 'NO CONTROL', array( 'fg' => 'red' ) ) . ' over running server apart from killing the V3 process' . PHP_EOL .
                            'So only use Daemon mode if you are 99.9% sure, that the server works fine without it.' . PHP_EOL .
                            'Choose your destiny.' . PHP_EOL
			) )
			{
				v3::log( 'Goin daemon..' );
				V3::terminate();
				$blnTerm = true;
			}
			else
			{
				V3::log( 'Running realtime mode..' );
			}
		}

		return $blnTerm;
	}

	public function getUptime($blnFormat = false )
	{
		$intUptime = time() - $this -> intStartTime;
		if ( !$blnFormat )
		{
			return $intUptime;
		}

		$intD = 0;
		$intH = 0;
		$intM = 0;
		$intS = 0;
		$strRet = '';

		$intD = floor( $intUptime / 60 / 60 / 24 );

		if ( $intD > 0 )
		{
			$strRet .= $intD . 'd ';
			$intUptime -= $intD * 60 * 60 * 24;
		}

		$intH = floor( $intUptime / 60 / 60 );
		if ( $intH > 0 )
		{
			$strRet .= $intH . 'h ';
			$intUptime -= $intH * 60 * 60;
		}

		$intM = floor( $intUptime / 60 );
		if ( $intM > 0 )
		{
			$strRet .= $intM . 'm ';
			$intUptime -= $intM * 60;
		}

		$strRet .= $intUptime . 's';

		return $strRet;
	}

	public function doIntervals()
	{
		foreach ( $this -> arrIntervals as $strHandle => &$objInterval )
		{
			if ( (!isset( $objInterval -> module ) AND ! isset( $objInterval -> reference )) OR ! $objInterval -> method OR ! $objInterval -> interval )
			{
				V3::log( 'Use of uninitialized interval "' . $strHandle . '", removing.', V3::NOTICE );
				$this -> delInterval( $strHandle );
				continue;
			}
			if ( time() - $objInterval -> interval >= $objInterval -> last )
			{
				if ( !empty( $objInterval -> suspended ) )
				{
					continue;
				}

				$objInterval -> calls++;
				if ( $objInterval -> max_calls > 0 AND $objInterval -> calls > $objInterval -> max_calls )
				{

					$this -> delInterval( $strHandle );
					continue;
				}
				$objInterval -> last = time();
				$objInterval -> call( );
			}
		}
	}

	public function addInterval($strHandle, V3_Interval $objI )
	{
		$objI->setHandle($strHandle); // interval should know its handle
		$this -> arrIntervals[$strHandle] = $objI;
		return true;
	}

	public function delInterval($strHandle )
	{
		if ( isset( $this -> arrIntervals[$strHandle] ) )
		{
			$clone = clone $this -> arrIntervals[$strHandle];
			unset( $this -> arrIntervals[$strHandle] );
			$clone -> __ondestruct( );
			unset( $clone );
			return true;
		}
		return false;
	}

	public function initialize($blnRegister = true )
	{
		if( !$this -> intStartTime )
		{
			$this -> intStartTime = time();
		}
		$this -> registerModule( 'Debug' );
		if ( $blnRegister )
		{
			$this -> registerModule( 'Config' );
			$this -> registerModules( );
		}

		V3_Mixins::register(  '_global_', 'persist', $this , 'persist' );
		V3_Mixins::register(  '_global_', 'retrieve', $this , 'retrieve' );
		V3_Mixins::register(  '_global_', 'unlink', $this , 'unlink' );
	}

	public function isTerminated()
	{
		return $this -> blnTerminated;
	}

	public function terminate($strReason = 'No reason.' )
	{
		V3::log( 'Terminate requested: ' . $strReason );
		$this -> blnTerminated = true;
	}

	public function getConfig()
	{
		return $this -> getModule( 'Config' );
	}

	public function registerModules()
	{
		if ( !$this -> isRegistered( 'Config' ) )
		{
			V3::log( 'Can\'t do V3_Core::registerModules() without Config module registered', V3::WARNING );
			return false;
		}

		foreach ( $this -> getConfig( ) -> getConf('App','modules') as $strModule )
		{
			$this -> registerModule( $strModule );
		}
		return true;
	}

	public function registerModule($strName, $arrParams = array( ) )
	{
		V3::log( 'Registering module ' . $strName, V3::DEBUG );

		$strClass = 'V3_' . $strName . 'Module';
		if ( !class_exists( $strClass ) )
		{
			V3_Loader::import( 'modules.'.$strName );
		}

		if ( !class_exists( $strClass ) )
		{
			throw new Exception( sprintf( 'Unable to find class %s for module %s', $strClass, $strName ) );
		}

		$objModule = new $strClass();
		if ( !$objModule instanceof V3_Module )
		{
			throw new Exception( sprintf( 'Class %s for module %s is not subclass of V3_Module', $strClass, $strName ) );
		}

		try
		{

			$objModule -> configure( $arrParams );

			$objModule -> initialize( $strName );
			$this -> arrModules[$strName] = array(
                'object' => $objModule,
                'name' => $strName,
                'version' => $objModule -> version,
                'compat' => $objModule -> compat,
                'author' => $objModule -> author,
                'events' => $objModule -> events,
                'deps' => $objModule -> deps,
                'can_unreg' => true
			);

			$objModule -> handleEvent( new V3_Event( 'activate' ), true );
			V3::castEvent( 'module_registered', array( 'name' => $strName, 'object' => $objModule ) );
			$objModule -> handleEvent( new V3_Event( 'post_activate' ) );
			V3::log( 'Registered module ' . $strName, V3::NOTICE );
		}
		catch ( Exception $e )
		{
			throw new Exception( 'Module failed to initialize(): ' . $e -> getMessage( ) );
		}
	}

	public function __destruct()
	{
		$this -> cleanup( );
	}

	public function cleanup()
	{
		V3::log( 'Cleaning up intervals', V3::NOTICE );
		foreach ( array_keys( $this -> arrIntervals ) as $strHandle )
		{
			$this -> delInterval( $strHandle );
		}
		V3::log( 'Unregistering modules', V3::NOTICE );
		foreach ( array_keys( array_reverse( $this -> arrModules ) ) as $strM )
		{
			$this -> unregisterModule( $strM );
		}
	}

	public function unregisterModule($strName )
	{
		if ( !$this -> isRegistered( $strName ) )
		{
			return false;
		}
		if ( !$this -> arrModules[$strName]['can_unreg'] )
		{
			V3::log( 'Can\'t unregister module ' . $strName, V3::WARNING );
			return false;
		}
		$this -> notify( $strName, 'deactivate' );
		$this -> castEvent( 'module_unregistered', array( 'name' => $strName ) );

		V3::log( 'Unregistered module ' . $strName, V3::NOTICE );
		unset( $this -> arrModules[$strName] );
		return true;
	}

	private function argumentCall( array $arrArgs )
	{
		foreach( array_keys( $arrArgs ) as $strArg )
		{
			if( method_exists( $this , 'handleArgument'. ucfirst($strArg) ))
			{
				call_user_func( array( $this, 'handleArgument'.ucfirst($strArg)));
			}
		}
	}

	private function handleArgumentV()
	{
		$this -> handleArgumentVersion(); /* duh! */
	}

	public function handleArgumentVersion()
	{
		V3::log( 'V3 Version: '. pakeColor::colorize(V3::BOT_VERSION, array('fg'=>'green')));
	}

	public function executeCommand($strCommand )
	{
		$strCommand = ucfirst( $strCommand );

		/** Argument without command support */
		if( in_array( $strCommand[0], array( '_', '-' ) ) )
		{
			$objCli = $this -> getModule( 'CLI' );
			$objCli -> parseArgs(false); /* Refresh */
			$this -> initialize( false );
			$this -> argumentCall( $objCli -> getArguments());
			
			return false;
		}

		V3::log( 'Executing command "' . $strCommand . '"' );
		V3_Loader::import( 'tasks.'.$strCommand );

		$objTask = V3::taskFactory( $strCommand );
		if ( !$objTask instanceof V3_Task )
		{
			throw new Exception( 'Something got REALLY messed up: the resulting class is not descendant of V3_Task' );
		}


		if ( !$objTask -> initialize( ) )
		{
			V3::log( 'Task couldn\'t initialize, exitting. Check exception for more info.', V3::FATAL );
			return false;
		}


		return $objTask -> execute( V3::getModule( 'CLI' ) );
	}

	public function breakWait ()
	{
		// cancels waiting for current event
		$this->_break_wait = true;
		return true;
	}

	public function removeWaitBreak ()
	{
		if (isset($this->_break_wait))
		{
			unset($this->_break_wait);
			return true;
		}
		return false;
	}

	public function waitForEvent( $strEvent = null )
	{
		if( $strEvent )
		{
			$this -> _blocking_event = $strEvent;
		}
		
		$this -> removeWaitBreak();
		
		while (! $this->isTerminated())
		{
			if (isset($this->_break_wait))
			{
				unset($this->_break_wait);
				$this->_blocking_event = null;
				$this->_got_event = null;
				return false;
			}
			if (! empty($this->_got_event))
			{
				$objEv = clone $this->_got_event;
				$this->_blocking_event = null;
				$this->_got_event = null;
				return $objEv;
			}
			$this->castEvent('idle');
			$this->doIntervals();
			usleep(20000);
		}
	}

	public function castEvent ($strName, $arrParams = array(), $blnPopulate = true, $objCaller = null )
	{
		if( is_null( $objCaller ) )
		{
			$objCaller = V3::getCallingObject();
		}

		if (! isset($this->eventList[$strName]))
		{
			$this->eventList[$strName] = 1;
		}
		else
		{
			$this->eventList[$strName] ++;
		}
		$objEvent = new V3_Event($strName, $arrParams, $blnPopulate, $this->eventList[$strName], $objCaller);
		if (isset($this->_blocking_event))
		{
			if ($this->_blocking_event == $strName)
			{
				// put it on pile and dont propagate further
				$this->_got_event = $objEvent;
				return true;
			}
		}
		else
		{
			if ($strName == 'idle')
			{
				$this->doIntervals();
			}
		}
		//echo "\r\n\r\n".$strName."\r\n\r\n";
		foreach ($this->arrModules as $arrModule)
		{
			if ($objEvent->getPopulate())
			{
				$arrModule['object']->handleEvent($objEvent);
			}
		}
		return true;
	}

	public function notify ($mixRcpt, $strName, $arrParams = array(), $objCaller = null )
	{
		if( is_null( $objCaller  ) )
		{
			$objCaller = V3::getCallingObject();
		}

		if (! isset($this->eventList[$strName]))
		{
			$this->eventList[$strName] = 1;
		}
		else
		{
			$this->eventList[$strName] ++;
		}
		$objEvent = new V3_Event($strName, $arrParams, false, $this->eventList[$strName], $objCaller);
		if (! is_array($mixRcpt))
		{
			$mixRcpt = array($mixRcpt);
		}
		foreach ($mixRcpt as $strModule)
		{
			if ($this->isRegistered($strModule))
			{
				$this->getModule($strModule)->handleEvent($objEvent);
			}
		}
		// if( unable_to_notify ) return false
		return true;
	}

	public function isRegistered($strModule )
	{
		return isset( $this -> arrModules[$strModule] );
	}

	public function getModule($strModule )
	{
		if ( isset( $this -> arrModules[$strModule] ) )
		{
			return $this -> arrModules[$strModule]['object'];
		}

		return false;
	}
}

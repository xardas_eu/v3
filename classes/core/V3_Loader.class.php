<?php
/**
 * 
 * V3 library loading subsystem. Supports recursion with *
 * @author xardas
 * @package V3
 * @subpackage Core
 *
 */
abstract class V3_Loader
{
	private static $arrImports = array();
	private static $strRootDir = '';
	private static $blnDebug = false;

	/** 
	 * 
	 * File masks to try to include. In probability order
	 * @var array
	 */
	private static $arrFileTypes = array(
		'V3_%s.class.php',
		'%s.class.php',
		'V3_%s.module.php',
		'V3_%s.task.php',
		'%s.php'
		);

		public static function setDebug( $blnDebug )
		{
			self::$blnDebug = (bool)$blnDebug;
		}

		public static function initialize( $strDir )
		{
			if( !is_dir( $strDir ) OR !is_readable( $strDir ))
			{
				throw new Exception( sprintf( 'Directory "%s" does not exist/is not readable.', $strDir ) );
			}
			else
			{
				self::$strRootDir = $strDir;
			}

		}

		private static function loadFile($strFile)
		{

			if(  !in_array( $strFile, self::$arrImports ) )
			{
				require_once $strFile;
				self::$arrImports[] = $strFile;
				if( self::$blnDebug )
				{
					echo 'V3_Loader::info: File "'. $strFile.'" has been loaded.'."\r\n";
				}
			}
		}

		public static function import()
		{
			$arrImports = func_get_args();
			foreach( $arrImports as $strImport )
			{
				$arrImport = explode( '.', $strImport );
				$strPath = self::$strRootDir;
				$intCount = sizeof( $arrImport );
				foreach( $arrImport as $intI => $strSegment )
				{
					$intI++;
					if( $strSegment != '*' )
					{
						if( $intI < $intCount )
						{
							$strPath .= DIRECTORY_SEPARATOR. $strSegment;

							if( !is_readable( $strPath ) )
							{
								throw new Exception( sprintf( 'The path name "%s" is not readable!', $strPath ) );
							}
						}
						else
						{
							$blnLoaded = false;
							foreach( self::$arrFileTypes as $strFileType )
							{
								$strFile = $strPath.DIRECTORY_SEPARATOR.sprintf( $strFileType, $strSegment );
								if( is_readable( $strFile ) )
								{
									self::loadFile($strFile);
									$blnLoaded = true;
									break;
								}
							}

							if( !$blnLoaded )
							{
								throw new Exception( sprintf( 'Could not load "%s"', $strPath.'.'.$strSegment ) );
							}
						}
					}
					else
					{
						$objIter = new FilesystemIterator( $strPath, FilesystemIterator::KEY_AS_FILENAME );
						foreach( $objIter as $objFile )
						{
							$strFilename = $objFile -> getFilename();
							$strExtension = end( explode( '.', $strFilename ) );
							if( $objFile -> isDir() )
							{
								self::import( $strPath.'.'. $strFilename.'.*' );
							}
							else
							{
								if( $strExtension == 'php' )
								{
									$strFile = $strPath.DIRECTORY_SEPARATOR.$strFilename;
									self::loadFile( $strFile );
								}
							}
						}
					}
				}
			}
		}
}

<?php
/**
 *
 * Implementation of intervals. Any module can register any number of them, they are run every x seconds
 * with possibility of limiting times it will run.
 * Also supports suspending and unsuspending (to save CPU cycles when unneeded)
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */

class V3_Interval extends V3_Accessors
{

	private $strHandle;

	public function setHandle( $strHandle )
	{
		$this -> strHandle = $strHandle;
	}

	public function getHandle()
	{
		return $this -> strHandle;
	}


	public function __construct()
	{
		$this->first = 0;
		$this->last = 0;
		$this->calls = 0;
		$this->max_calls = 0;
		$this->args = array ();
	}

	public function removeItself()
	{
		$this -> getCore() -> delInterval( $this -> getHandle() );
	}

	public function __toString()
	{
		return $this -> getHandle();
	}

	public function onDelete($strModule, $strFunction)
	{
		$this->dest_module = $strModule;
		$this->dest_method = $strFunction;
	}

	public function __ondestruct()
	{
		if( empty($this->dest_calls))
		{
			$this->dest_calls=1;
		}
		if (!empty( $this->arrProperties ['dest_module'] ))
		{
			$this->module = $this->dest_module;
			$this->method = $this->dest_method;
			$this->call (true);
		}


		$this->dest_calls++;
	}

	public function suspend()
	{
		$this -> suspended = true;
	}

	public function unsuspend()
	{
		$this -> suspended = false;
	}

	public function setCallable($strModule, $strFunction)
	{
		$this->module = $strModule;
		$this->method = $strFunction;

		$objModule = $this->getModule ( $this->module );
		if (! is_callable ( array ($objModule, $this->method ) ))
		{
			throw new Exception ( sprintf ( 'Function %s::%s specified for interval is not callable!', $strModule, $strFunction ) );
		}
	}

	public function setCallableByRef(V3_Accessors $objReference, $strFunction)
	{
		$this -> reference = $objReference;
		$this -> method = $strFunction;
	}

	public function call($blnDestructor=false)
	{
		if( $this -> calls == 1)
		{
			$this->__set ( 'args', array_merge ( $this->args, array ($this ) ) );
		}

		if (! empty ( $this->module ))
		{
			$objModule = $this->getModule ( $this->module );

			if (is_callable ( array ($objModule, $this->method ) ))
			{
				call_user_func_array ( array ($objModule, $this->method ), $this->args );
			}
			else
			{
				V3::log ( get_class ( $objModule ) . '::' . $this->method . '() is not callable.', V3::WARNING );
			}
		}
		elseif (! empty ( $this->reference ))
		{
			if (is_callable ( array ($this->reference, $this->method ) ))
			{
				call_user_func_array ( array ($this->reference, $this->method ), $this->args );
			}
			else
			{
				V3::log( get_class($this->reference). '::'. $this->method. '() is not callable.', V3::WARNING );
			}
		}

		if( $blnDestructor )
		{
			$this -> reference = null;
			$this -> module    = null;
			$this -> method    = null;
			$this -> dest_module = null;
			$this -> dest_method = null;
		}
	}

	public function setArgs($arrArgs = array())
	{
		$this->args = $arrArgs;
	}

	public function setMaxCalls($intCalls = 1)
	{
		$this->max_calls = $intCalls;
	}

	public function setInterval($intSec = 1)
	{
		$this->interval = $intSec;
		$this->last = time ();
	}

	public function getInterval()
	{
		return $this -> interval;
	}
}
?>

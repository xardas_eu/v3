<?php
/**
 *
 * Implementation of PHP shared memory - to be used with several instances of V3 running on one machine.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
class V3_SharedMemory
{
	const SHARED_MEMORY_SIZE = 1024; /* in KBytes */
	
	private $strMemory = null; /* serialized shared memory */
	private $arrMemory = array(); /* array of shared variables */
	
	private $intMemoryId = 0;
	
	public function initialize( $strKey )
	{
		$strKey = ftok( $strKey, 'V' );
		if( $intId = shmop_open( $strKey, 'w', 0777, self::SHARED_MEMORY_SIZE*1024  ) !== false )
		{
			
		}
	}
	
}
?>
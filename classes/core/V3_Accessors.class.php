<?php
/**
 *  Some kind of master-class. Provides convenience methods that can be used virtually EVERYWHERE,
 *  as it takes care for the scope.
 *  Class that defines all the common accessors/proxy methods.
 *	All V3 non-abstract classes should inherit from it.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
abstract class V3_Accessors
{
	/**
	 *
	 * Array holding every object properties - handled by
	 * @see __get
	 * @see __set
	 *
	 * @var array
	 */
	public $arrProperties;

	/**
	 *
	 * Opens file resource (for module) for read/write access
	 * @param string $strPath
	 * @return boolean|array $arrTemp
	 *
	 */
	public function loadResource( $strPath )
	{
		if( !file_exists( $strPath ) )
		{
			@file_put_contents( $strPath, null );
		}

		if( is_file( $strPath ) )
		{
			V3::log( sprintf( 'Loading resource "%s" for module %s', $strPath, $this -> getName() ), V3::INFO );
			$this -> resource_path = $strPath;
				
			$strTemp = $this -> arc4Encrypt( file_get_contents( $strPath ) );
			$arrTemp = @unserialize( $strTemp );
			if( !is_array( $arrTemp ) )
			{
				$this -> saveResource();
			}
				
			return (array)$arrTemp;
		}

		V3::log( 'The resource: "'.$strPath.'" is not readable.', V3::WARNING );
		return false;
	}

	/**
	 *
	 * Saves file resource for module
	 * @param array $arrData
	 * @return boolean
	 */
	public function saveResource( $arrData = array() )
	{
		if( !isset( $this -> resource_path ) )
		{
			V3::log( 'Wanted to save resource for module '. $this -> getName().' but it didn\'t open any.', V3::WARNING );
			return false;
		}
		V3::log( sprintf( 'Saving resource "%s" for module %s', $this -> resource_path, $this -> getName()), V3::NOTICE );
		$strText = $this -> arc4Encrypt( serialize( $arrData ) );
		return file_put_contents( $this -> resource_path, $strText );
	}

	/**
	 *
	 * Sets event nama for pseudoblocking events
	 * @param string $strEvent
	 * @return void
	 */
	public function setWaitingFor( $strEvent )
	{
		V3::getCore() -> _blocking_event = $strEvent;
		V3::getCore() -> removeWaitBreak();
	}

	public function breakWait()
	{
		return V3::getCore() -> breakWait();
	}

	/**
	 *
	 * Returns event that was set for waiting.
	 * Until then, still executes main loop.
	 * @see setWaitingFor
	 * @return V3_Event $e
	 */
	public function waitForEvent( $strEvent = null )
	{
		return V3::getCore() -> waitForEvent( $strEvent );
	}

	/**
	 *
	 * Sets Arc4 key for in-module encryption
	 * @param string $strKey
	 * @return boolean
	 */
	public function arc4SetKey( $strKey )
	{
		// setting is per-module
		$objArc = $this -> getArc4();
		if( !$objArc instanceof V3_Arc4Module )
		{
			return false;
		}

		return $objArc -> setKey( $this -> getName(), $strKey );
	}

	/**
	 *
	 * Encrypts or decrypts given string with chosen key
	 *
	 * @see arc4SetKey
	 * @param string $strText
	 * @return string $strText
	 */
	public function arc4Encrypt( $strText )
	{
		$objArc = $this -> getArc4();
		if( !$objArc instanceof V3_Arc4Module )
		{
			return $strText;
		}

		return $objArc -> encryptDecrypt( $this -> getName(), $strText );
	}

	/**
	 *
	 * Returns V3_Core instance
	 * @return V3_Core $core
	 */
	protected static function getCore()
	{
		return V3::getCore();
	}

	/**
	 *
	 * Proxy method for terminating the application
	 * @param string $strReason Terminate reason
	 */
	public static function terminate( $strReason = 'No reason.' )
	{
		self::getCore() -> terminate( $strReason );
	}

	/**
	 *
	 * For use in custom objects (descendants of V3_Accessors) constructors.
	 * Sets all object properties from array passed to object constructor.
	 * Constructor must explicitly call $this->registerProperties($arrData)
	 * @param $arrData
	 */
	public function registerProperties( $arrData )
	{
		foreach( $arrData as $strKey => $strVal )
		{
			$this -> __set( $strKey, $strVal );
		}
	}

	/**
	 *
	 * Magic method for module getters and/or Mixins
	 *
	 * @see V3_Mixins
	 * @param string $strFunc
	 * @param array $arrArgs
	 * @throws Exception
	 * @return mixed $mixResult
	 */
	public function __call( $strFunc, $arrArgs )
	{
		$strPrefix = substr( $strFunc, 0, 3 );
		$strWhat   = substr( $strFunc, 3 );

		switch( $strPrefix )
		{
			case 'get':
				return $this -> getModule( $strWhat );
				break;
		}

		/* Mixins functionality */

		$callable = V3_Mixins::getCallable( get_class($this), $strFunc );
		if( $callable )
		{
			return V3_Mixins::call( $callable, $arrArgs );
		}

		throw new Exception( sprintf( 'Call to undefined method %s(*) of %s ', $strFunc, get_class( $this ) ) );

	}

	/*
	 * Hack to use !something() or $this->throwException(...)
	 * @param string $strMsg
	 * @throws Exception
	 * */
	public function throwException( $strMsg )
	{
		throw new Exception( $strMsg );
	}

	public function __set( $strVariable, $strValue )
	{
		$this -> arrProperties[ $strVariable ] = $strValue;
	}

	public function hasProperty( $strVariable )
	{
		return isset( $this -> arrProperties[ $strVariable ] );
	}

	public function __isset( $strVariable )
	{
		return $this -> hasProperty( $strVariable );
	}

	public function __unset( $strVariable )
	{
		if( $this -> hasProperty( $strVariable ) )
		{
			unset( $this -> arrProperties[ $strVariable ] );
			return true;
		}
		return false;
	}

	public function delProperty( $strVariable )
	{
		if( $this -> hasProperty( $strVariable ) )
		{
			unset( $this -> arrProperties[ $strVariable ] );
		}
	}

	public function __get( $strVariable )
	{
		if( isset( $this -> arrProperties[ $strVariable ] ) )
		{
			return $this -> arrProperties[ $strVariable ];
		}

		V3::log( 'Undefined property: '. get_class( $this ).'::'. $strVariable, V3::WARNING );
		return false;
	}


	public static function castEvent( $strName, $arrParams = array(), $blnPopulate = true )
	{
		if( empty( V3::$objCore ) )
		{
			return false;
		}
		return self::getCore() -> castEvent( $strName, $arrParams, $blnPopulate );
	}

	public static function notify( $mixRcpt, $strName, $arrParams = array() )
	{
		if( empty( V3::$objCore ) )
		{
			return false;
		}
		return self::getCore() -> notify( $mixRcpt, $strName, $arrParams );
	}

	public function _getConf( $strModule, $strVar, $strDefault = false )
	{
		if( $this -> getCore() )
		{
			if( ( $objConf = $this -> getCore() -> getModule( 'Config') ) !== false )
			{
				$strVal = $objConf -> getConf( $strModule, $strVar );
				if( empty( $strVal ) )
				{
					return $strDefault;
				}
				return $strVal;
			}
		}
		return $strDefault;
	}

	public function getAppConf( $strVar, $strDefault = false )
	{
		return $this -> getConf( 'App', $strVar, $strDefault );
	}

	public function runOnce( $strModule, $strMethod, $intInterval )
	{
		return $this -> runEvery( $strModule, $strMethod, $intInterval, 1 );
	}

	public function runEvery( $strModule, $strMethod, $intInterval, $intMaxCalls = 0, $arrArgs = array() )
	{
		$i = new V3_Interval();
		if( !is_object( $strModule ) )
		{
			$i -> setCallable( $strModule, $strMethod );
		}
		else
		{
			$i -> setCallableByRef( $strModule, $strMethod );
			$strModule = get_class( $strModule );
		}
		$i -> setMaxCalls( $intMaxCalls );
		$i -> setArgs( $arrArgs );
		$i -> setInterval( $intInterval );

		$this -> addInterval( md5(time().$strModule.$strMethod), $i );
		return $i;
	}

	public function addInterval( $strHandle, V3_Interval $objI )
	{
		return $this -> getCore() -> addInterval( $strHandle, $objI );
	}

	public function delInterval( $strHandle )
	{
		return $this -> getCore() -> delInterval( $strHandle );
	}

	public function getConfig()
	{
		return $this -> getModule( 'Config' );
	}

	public function getModuleConf( $strVar, $strDefault = false )
	{
		if( !$this instanceof V3_Module )
		{
			return false;
		}

		V3::log( sprintf( 'V3_Accessors::getModuleConf(%s,%s)', $this->getName(),$strVar), V3::VERBOSE );
		return self::_getConf( $this -> getName(), $strVar, $strDefault );
	}

	/* Proxy method */
	public static function getModule( $strModule )
	{
		return self::getCore() -> getModule( $strModule );
	}

	public function isRegistered( $strModule )
	{
		return self::getCore() -> isRegistered( $strModule );
	}
}

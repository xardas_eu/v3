<?php
/**
 *
 * Provides mixins functionality - some kind of atub for multi-inheritance. Allow any class method to be called
 * from any class as it was that class's own method.
 *
 * @author xardas
 * @package V3
 * @subpackage Core
 */
abstract class V3_Mixins
{
	private static $arrMixins = array( '_global_' => array()  );
	
	
	public static function getMixins()
	{
		return self::$arrMixins;
	}
	
	public static function register( $localClass, $localMethod, $remoteObject, $remoteMethod )
	{
		if( !isset( self::$arrMixins[$localClass] ) )
		{
			self::$arrMixins[$localClass] = array();
		}
		
		self::$arrMixins[$localClass][$localMethod] = array( $remoteObject, $remoteMethod );
	}
	
	public static function call( $callable, $arguments )
	{
		return call_user_func_array($callable, $arguments);
	}
	
	public static function getCallable( $localClass, $localMethod )
	{
		if( isset( self::$arrMixins[$localClass] ) )
		{
			if( isset( self::$arrMixins[$localClass][$localMethod] ) )
			{
				return self::$arrMixins[$localClass][$localMethod];
			}
		}
		
		/**
		 * 
		 * Mixins that are available app-wide (in every class derived from V3_Accessors
		 */
		
		$localClass = '_global_';
		if( isset( self::$arrMixins[$localClass] ) )
		{
			if( isset( self::$arrMixins[$localClass][$localMethod] ) )
			{
				return self::$arrMixins[$localClass][$localMethod];
			}
		}
		
		return false;
	}
}
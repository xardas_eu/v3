<?php
/**
 *
 * Facade for every V3 task. Provides some convenience method, also sets requirements that every task must
 * pass to be able to run.
 *
 * @author xardas
 * @package V3
 * @subpackage App
 */
class V3_Task extends V3_Accessors
{

	public function execute(V3_CLIModule $objCli ) {
		foreach ((array) $this->required_params as $strParam => $strDescription) {
			if (( $strValue = $objCli->getArgument($strParam) ) === false) {
				throw new Exception(sprintf('Required parameter "--%s" (%s) omitted, exitting.', $strParam, $strDescription));
			}
			$this->__set('arg_' . $strParam, $strValue);
		}

		if (!is_callable(array($this, '_exec'))) {
			throw new Exception(get_class($this) . ' does not provide the _exec method');
		}

		$this->_exec();
	}

	public function initialize() {
		try {
			if (!is_callable(array($this, '_init'))) {
				throw new Exception(get_class($this) . ' does not provide the _init method');
			}

			if (!$this->_init()) {
				throw new Exception(get_class($this) . ' internal _init() failed');
			}

			if ($this->required_params === false) {
				throw new Exception(get_class($this) . ' did not set its required parameters.');
			}

			if (!$this->author) {
				throw new Exception(get_class($this) . ' did not set its author');
			}

			if (!$this->description) {
				throw new Exception(get_class($this) . ' did not set its description');
			}

			if (!$this->version) {
				throw new Exception(get_class($this) . ' did not set its version');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage() . '. initialize() failed, exitting.');
		}

		return true;
	}

}

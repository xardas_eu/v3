<?php
/**
 *
 * Facade for every V3 module. Provides some convenience method, also sets requirements that every module must
 * pass to be able to run.
 *
 * @author xardas
 * @package V3
 * @subpackage App
 */
abstract class V3_Module extends V3_Accessors
{
	public function isEventHandler($strEvent, $strFunction)
	{
		if( !isset( $this -> events[ $strEvent ] ) )
		{
			$this->__set ( 'events', array_merge ( $this->events, array ($strEvent => array () ) ) );
		}

		if( !is_array( $this -> events[$strEvent]))
		{
			return true;
		}
		return in_array ( $strFunction, $this->events[$strEvent] );
	}

	public function registerEventHandler($strEvent, $strFunction)
	{
		if (! $this->isEventHandler ( $strEvent, $strFunction ))
		{
			$this->__set ( 'events', array_merge ( $this->events, array ($strEvent => array ($strFunction ) ) ) );
			//			$this -> events[ $strEvent ][] = $strFunction;
		}
		return;
	}

	public function writeToOwnSocket( $strData, $blnAppendCrlf = true )
	{

	}

	public function socketBind( $strHost, $intPort, $intMode = V3::SOCKET_TEXT_READ, $intMaxConn = V3_SocketModule::MAX_CONN_QUEUE )
	{
		$objSock = $this -> getModule( 'Socket' );
		if( !$objSock instanceof V3_Module )
		{
			return false;
		}

		$objSocket = $objSock -> bind( $strHost, $intPort, $this -> getName(), $intMode, $intMaxConn );
		if( $objSocket )
		{
			$this -> registerEventHandler( 'socket_read', 'socket_read' );
			$this -> registerEventHandler( 'socket_bound', 'socket_bound' );
			$this -> registerEventHandler( 'socket_accepted', 'socket_accepted' );
			$this -> registerEventHandler( 'socket_closed', 'socket_closed' );
			return $objSocket;
		}

		return false;
	}

	public function socketConnect( $strHost, $intPort, $intMode = V3::SOCKET_TEXT_READ, $strVhost = '' )
	{
		$objSock = $this -> getModule( 'Socket' );
		if( !$objSock instanceof V3_Module )
		{
			return false;
		}

		$objSocket = $objSock -> connect( $strHost, $intPort, $this -> getName(), $intMode, $strVhost );
		$this -> registerEventHandler( 'socket_read', 'socket_read' );
		$this -> registerEventHandler( 'socket_closed', 'socket_closed' );
		return $objSocket;
	}

	public function unregisterEventHandler($strEvent, $strFunction = null)
	{
		/* If $strFunction == null then remove all handlers for that event */
		if ($strFunction == null)
		{
			if (isset ( $this->events [$strEvent] ))
			{
				unset ( $this->events [$strEvent] );
			}
			return;
		}

		if (isset ( $this->events [$strEvent] [$strFunction] ))
		{
			unset ( $this->events [$strEvent] [$strFunction] );
		}
		return;
	}

	public function getName()
	{
		return $this->strName;
	}

	public function handleEvent(V3_Event $objE, $blnAllowNoHandle=false)
	{
		if (! isset ( $this->events [$objE->getName ()] ))
		{
			return false;
		}

		if (! is_array ( $this->events [$objE->getName ()] ))
		{
			if (is_callable ( array ($this, 'handle_' . $objE->getName () ) ))
			{
				return call_user_func_array ( array ($this, 'handle_' . $objE->getName () ), array ($objE ) );
			}
			else
			{
				if( $blnAllowNoHandle )
				{
					return false;
				}
				throw new Exception( sprintf( '%s: default event handler for event "%s" is not callable.', get_class( $this ), $objE -> getName() ) );
			}
		}

		foreach ( ( array ) $this->events [$objE->getName ()] as $strHandler )
		{
			if (is_callable ( array ($this, $strHandler ) ))
			{
				//handle
				call_user_func_array ( array ($this, $strHandler ), array ($objE ) );
			}
			else
			{
				//die('none auto chance balance' );
				if( $blnAllowNoHandle )
				{
					return false;
				}
				throw new Exception ( sprintf ( '%s: registered event handler "%s" for event "%s" is not callable.', get_class ( $this ), $strHandler, $objE->getName () ) );
			}
		}
	}

	/* Shortcut */
	public function getConf($strVar, $strDefault )
	{
		return $this->getModuleConf ( $strVar, $strDefault );
	}


	public function configure( array $arrParams )
	{

		if( method_exists( $this, '_config' ) )
		{
				
			$this -> _config( $arrParams );
		} else {
			// setting required fields
			foreach( $arrParams as $mixParam => $mixValue ) {
				$this -> $mixParam = $mixValue;
			}
		}


	}

	public function initialize($strName)
	{
		$this->strName = $strName;
		try
		{
			if (! is_callable ( array ($this, '_init' ) ))
			{
				throw new Exception ( get_class ( $this ) . ' does not provide the _init method' );
			}

			if (! $this->_init ())
			{
				throw new Exception ( get_class ( $this ) . ' internal _init() failed' );
			}

			$this->author or $this->throwException ( get_class ( $this ) . ' did not set its author' );
			$this->version or $this->throwException ( get_class ( $this ) . ' did not set its version' );
			$this->deps or $this->throwException ( get_class ( $this ) . ' did not set its dependencies' );
			$this->compat or $this->throwException ( get_class ( $this ) . ' did not set its compatibility' );
			is_array ( $this->events ) or $this->throwException ( get_class ( $this ) . ' did not set its events' );

			version_compare ( $this->compat [0], V3::BOT_VERSION, '<=' ) && version_compare ( $this->compat [1], V3::BOT_VERSION, '>=' ) or $this->throwException ( get_class ( $this ) . sprintf ( ' is incompatible (%s-%s) with this (%s) version of V3', $this->compat [0], $this->compat [1], V3::BOT_VERSION ) );



			$this->__set ( 'events', $this->events + array ('activate' => array ('event_activate' ) ) );
				

			foreach( $this->events as $strEventName => $arrEventCallables )
			{

				foreach((array)$arrEventCallables as $strEventCallable)
				{
					if( is_bool( $strEventCallable ) ) {
						$strEventCallable = 'handle_'.$strEventName;
					}
						
					if( !method_exists( $this, $strEventCallable ) )
					{
						throw new Exception ( get_class ( $this ) . ': '.sprintf ( 'One of %s event handlers (%s) has been registered but callable (%s) not found.', $this->getName (), $strEventName, $strEventCallable ) );
						return;
					}
				}
			}
				
			foreach ( ( array ) $this->deps as $strDep )
			{
				if ($strDep == null)
				{
					continue;
				}
					
				if (! $this->isRegistered ( $strDep ))
				{
					throw new Exception ( get_class ( $this ) . ': '.sprintf ( 'One of %s dependencies (%s) is not met.', $this->getName (), $strDep ) );
					return;
				}
			}

		}
		catch ( Exception $e )
		{
			throw new Exception ( $e->getMessage () . '. initialize() failed, exitting.' );
			return false;
		}

		V3::log ( 'Module initialize() success.', V3::VERBOSE );
		return true;
	}
}

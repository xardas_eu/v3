<?php

/**
 * Base static class.
 * Mainly just:
 * - instance-holder
 * - constant-holder
 * - global-static-method-holder
 * - even that class extends V3_Accessors :)
 * 
 * @author xardas
 * @package V3
 */
abstract class V3 extends V3_Accessors
{
    /* Meatload of constants */

    const CRLF = "\r\n";
    const CH_ZERO = 0;
    const DEBUG_TIMESTAMP_FORMAT = 'H:i:s';
    const BOT_VERSION = '0.1.5';

	const IS_STABLE	= false;

    const INFO = 1;
    const NOTICE = 2;
    const WARNING = 4;
    const ERROR = 8;
    const FATAL = 16;
    const DEBUG = 32;
    const VERBOSE = 64;

    const SOCKET_TEXT_READ = 1;
    const SOCKET_BINARY_READ = 2;

    /*
     * Holds instance of V3 core
     * @var V3_Core $objCore
     */

    public static $objCore = false;

    /**
     * Normalizes string for use in IRC Raw lines
     * @param string $strText String to normalize
     * @return string $strText Normalized string
     */
    public static function normalize($strText )
    {
        return str_replace(
                array( '#', '~' ),
                '',
                strtolower( $strText )
        );
    }

    /**
     * Very fast and effective ircmask tokenizer, much faster than explode() or any regular expression
     *
     * @param string $strMask IRC Mask to tokenize
     * @return array|boolean Array on success, boolean on failuer
     */
    public static function explodeMask($strMask)
    {
        $strNick = strtok( $strMask, '!' );
        $strFoo = strtok( '!' );
        $strIdent = strtok( $strFoo, '@' );
        $strHost = strtok( '@' );

        if ( empty( $strNick ) || empty( $strIdent ) || empty( $strHost ) )
        {
            /* Incorrect hostmask, failing */
            return false;
        }
        return array( 'full' => $strNick . '!' . $strIdent . '@' . $strHost, 'nick' => $strNick, 'user' => $strIdent, 'host' => $strHost );
    }

    /**
     * Simple factory for CLI tasks.
     *
     * @param string $strTask
     * @throws Exception
     * @return V3_Task $objTask
     */
    public static function taskFactory($strTask)
    {
        if ( !class_exists( 'V3_' . $strTask . 'Task' ) )
        {
            throw new Exception( 'Task class V3_' . $strTask . 'Task does not exist so task object can not be created' );
        }

        $objTask = null;
        eval( '$objTask = new V3_' . $strTask . 'Task();' );
        return $objTask;
    }

    /**
     * IRC Raw messages parser (according to RFC)
     * Returns integer-indexed array with parts of command, or boolean false on failure
     *
     * @param string $strMsg
     * @return array|boolean Array on success, bool on failure
     */
    public static function parseRaw($strMsg)
    {
        if ( preg_match( '/^(\:.*?\s){0,1}([A-Z]+|[0-9]{1,3})\s{0,1}(.*?)(\s\:.*?){0,1}$/', $strMsg, $arrOut ) )
        {
            if ( isset( $arrOut [4] ) )
            {
                $arrOut [4] = substr( trim( $arrOut [4] ), 1 );
            }

            if ( strpos( $arrOut [3], ':' ) === 0 )
            {
                $arrOut [3] = substr( $arrOut [3], 1 );
            }
            unset( $arrOut [0] );
            ksort( $arrOut );
            return $arrOut;
        }
        else
        {
            /* Not proper raw string, failing */
            return false;
        }
    }
    
    
	public static function getCallingObject()
	{
		$arrBTrace = debug_backtrace( true );

                if( isset( $arrBTrace[2]['object'] ) )
                {
                        return $arrBTrace[2]['object'];
                }

		if( isset( $arrBTrace[1]['object'] ) )
		{
			return $arrBTrace[1]['object'];
		}
	}

    /**
     * Simple trace string generator for debugging
     * @param array $arrCall
     * @return string $ret
     */
    public static function generateTraceString($arrCall)
    {
        $ret = '';
        if ( isset( $arrCall ['class'] ) )
        {
            $caller = $arrCall ['class'] . $arrCall ['type'] . $arrCall ['function'] . '(*)';
        }
        else
        {
            $caller = $arrCall ['function'] . '(*)';
        }

        $ret .= $caller . ' -> ';
        return $ret;
    }

    /**
     * Returns brief info about function/method that called the current one
     * @return string
     */
    public static function debug_caller_name()
    {
        $_callArray = debug_backtrace ();
        $_callerArray = $_callArray [2];
        if ( isset( $_callerArray ['class'] ) )
        {
            $callerName = $_callerArray ['class'] . $_callerArray ['type'] . $_callerArray ['function'];
        }
        else
        {
            $callerName = $_callerArray ['function'];
        }
        return $callerName . '(c=' . count( $_callerArray ['args'] ) . ')';
    }

    /**
     * More advanced version of debug_caller_name.
     * Main difference is that this function returns full stack trace, including function argument dump
     * with proper colors and formatting. It is recommended to use this one if you need a full trace.
     *
     * @see V3::debug_caller_name
     * @return string $ret
     */
    public static function debug_caller_name2()
    {
        $_callArray = debug_backtrace ();
        $ret = 'TRACE:';
        array_shift( $_callArray );
        array_shift( $_callArray );
        $_callArray = array_reverse( $_callArray );

        $arrExclude = array( 'call_user_func_array', 'initialize', 'registerModules', 'execute', 'delInterval', 'handleEvent' );
        //$arrExclude = array( '_init', '_exec', 'log', 'initialize', 'executeCommand', 'execute', 'castEvent', 'call_user_func_array', 'delInterval', '__destruct' );
        $intPad = 0;
        foreach ( $_callArray as $arrCall )
        {
            $when_and_where = '';
            if ( in_array( $arrCall ['function'], $arrExclude ) )
            {
                continue;
            }

            $strArgs = '';
            if ( empty( $arrCall['args'] ) )
            {
                $arrCall['args'] = array( );
            }
            foreach ( $arrCall['args'] as $mixArgument )
            {
                $strType = gettype( $mixArgument );
                $strHint = null;

                switch ( $strType )
                {
                    case 'boolean':
                        $strHint = $mixArgument ? 'true' : 'false';
                        break;
                    case 'integer':
                    case 'float':
                    case 'double':
                        $strHint = $mixArgument;
                        break;
                    case 'string':
                        $strHint = '\'' . $mixArgument . '\'';
                        break;
                    case 'object':
                        $strHint = get_class( $mixArgument );
                        break;
                    case 'array':
                        $strHint = count( $mixArgument );
                        break;
                    default:
                        $strHint = '*';
                        break;
                }

                if ( $strHint )
                {
                    $strHint = '(' . $strHint . ')';
                }
                $strArgs .= $strType . $strHint . ', ';
            }

            $strArgs = pakeColor::colorize( rtrim( $strArgs, ', ' ), array( 'fg' => 'yellow' ) );


            pakeColor::style( 'red', array( 'fg' => 'red' ) );
            pakeColor::style( 'blue', array( 'fg' => 'blue' ) );
            pakeColor::style( 'green', array( 'fg' => 'green' ) );


			$objCli = self::getCore()->getModule('CLI'); /* @var V3_CLIModule */

          	if( isset( $arrCall['file']) && $objCli->hasArgument('trace-files') )
			{
				$when_and_where = ' ['.
						pakeColor::colorize(str_replace(V3_DIR.DIRECTORY_SEPARATOR,'',$arrCall['file']),'green').
						':'.
						pakeColor::colorize($arrCall['line'],'green').']';
			}

            if ( isset( $arrCall ['class'] ) )
            {
                $caller = pakeColor::colorize( $arrCall ['class'], 'blue' ) .
                        pakeColor::colorize( $arrCall ['type'], 'green' ) .
                        pakeColor::colorize( $arrCall ['function'], 'red' ) .
                        '( ' . $strArgs . ' )';
            }
            else
            {
                $caller = pakeColor::colorize( $arrCall ['function'], 'red' ) . '( ' . $strArgs . ' )';
            }

            $ret .= PHP_EOL .
                    str_repeat( ' ', $intPad ) .
                    $caller .
                    $when_and_where .
                    pakeColor::colorize( ' -> ', array( 'fg' => 'green' ) );
                    $intPad += 2;
        }
        unset( $_callArray, $arrCall, $caller );
        return $ret;
    }

    /**
     * Singleton register method, nothing to say.
     * @param V3_Core $objCore
     * @return void
     */
    public static function registerCore(V3_Core $objCore)
    {
        self::$objCore = $objCore;
    }

    /**
     * Proxy method interfacing with Debug module, enables the developer to log some messages
     *
     * Returns true on success, otherwise false.
     *
     * @see V3_DebugModule
     * @param string $strText
     * @param integer $intLevel
     * @return boolean
     */
    public static function log($strText, $intLevel = self::INFO)
    {
        if ( !self::getCore() || ! self::getCore () -> isRegistered( 'Debug' ) )
        {
			if( !in_array( $intLevel, array( self::NOTICE, self::WARNING, self::ERROR, self::FATAL) ) )
			{
				return;
			}


            /* Prints msg on output if no Debug module is loaded */

            if ( ( $objCore = self::getCore() ) != false )
            {
                if ( $objCli = $objCore -> getModule( 'CLI' ) )
                {
					if( $objCli -> hasArgument('q') OR $objCli -> hasArgument('quiet') )
					{
						return false;
					}

                    if ( $intLevel == V3::VERBOSE AND $objCore -> getModule( 'CLI' ) -> hasArgument( 'noverbose' ) )
                    {
                        return false;
                    }
                    if ( $intLevel == V3::DEBUG AND $objCore -> getModule( 'CLI' ) -> hasArgument( 'nodebug' ) )
                    {
                        return false;
                    }
                }
            }


            echo "\r" . 'V3::log: ' . $strText . self::CRLF;
        }
        self::castEvent( 'log', array( 'msg' => $strText, 'level' => $intLevel ) );
        return true;
    }

    /**
     * Proxy method for invoking simple unit-tests
     *
     * @see V3_DebugModule
     * @param string $strArg1 First argument to compare
     * @param string $strOperator The way we compare (like == or <>)
     * @param string $strArg2 Second argument to compare
     * @param string $strDescription Test description (as displayed in logs)
     */
    public static function test($strArg1, $strOperator = null, $strArg2 = null, $strDescription = 'no description.' )
    {
        self::notify( 'Debug', 'test', array( 'arg1' => $strArg1, 'operator' => $strOperator, 'arg2' => $strArg2, 'description' => $strDescription ) );
    }

    /**
     * Returns V3_Core instance
     *
     * @return V3_Core V3 Core instance
     */
    protected static function getCore()
    {
        return self::$objCore ? self::$objCore : false;
    }

    /**
     * Defines base constants ;)
     * I preferred to have it there, in one place.
     *
     * @return void
     */
    public static function defineBaseConstants()
    {
        define( 'CH_ZERO', chr( 1 ) );
        define( 'CH_COLOR', chr( 3 ) );
        define( 'CH_BOLD', chr( 2 ) );
        define( 'CH_REVERSE', chr( 22 ) );
    }

    /**
     * For easy bootstrapping. Loads all the classes we need to successfully run V3 platform.
     * @return void
     */
    public static function importBaseClasses()
    {
    	V3_Loader::import( 'lib.pake.pakecolor' );
    	V3_Loader::import( 'classes.core.*' );
    	V3_Loader::import( 'classes.app.*' );
    }

}
